import FWCore.ParameterSet.Config as cms

effiana2 = cms.EDAnalyzer('EfficiencyAnalyzer2',
    _trigResults = cms.InputTag("TriggerResults","","HLT"),
    _trigEvent = cms.InputTag("hltTriggerSummaryAOD","","HLT"),
    _muons = cms.InputTag("displacedStandAloneMuons"),
    _trigPath = cms.untracked.string('HLT_DoubleL2Mu23NoVtx_2Cha'),
    processName = cms.untracked.string("HLT"),
    _verbose = cms.untracked.bool(False)
)
