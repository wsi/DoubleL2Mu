import FWCore.ParameterSet.Config as cms

genana = cms.EDAnalyzer('GenAnalyzer',
    _trigResults = cms.InputTag("TriggerResults","","DisplacedTriggers"),
    _trigEvent = cms.InputTag("hltTriggerSummaryAOD","","HLT"),
    _genMuons = cms.InputTag('genParticles'),
    _dsaMuons = cms.InputTag("displacedStandAloneMuons"),
    _trigPath = cms.untracked.string('HLT_DoubleL2Mu25NoVtx_2Cha_v4'),
    nmuons = cms.untracked.uint32(2)
)
