import FWCore.ParameterSet.Config as cms

effiana = cms.EDAnalyzer('EfficiencyAnalyzer',
    _trigResults = cms.InputTag("TriggerResults","","HLT"),
    _trigEvent = cms.InputTag("hltTriggerSummaryAOD","","HLT"),
    _recoMuons = cms.InputTag("muons"),
    _dsaMuons = cms.InputTag("displacedStandAloneMuons"),
    _beamspot = cms.InputTag("offlineBeamSpot"),
    _trigPath = cms.untracked.string('HLT_DoubleL2Mu23NoVtx_2Cha_v'),
    _trigFilter = cms.untracked.InputTag("hltL2DoubleMu23NoVertexL2Filtered2Cha", "", "HLT"),
    nmuons = cms.untracked.uint32(2),
    mindxy = cms.untracked.double(0.)
)
