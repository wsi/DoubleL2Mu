import FWCore.ParameterSet.Config as cms
import FWCore.ParameterSet.VarParsing as VarParsing

process = cms.Process("USER")

options = VarParsing.VarParsing('analysis')
options.register('test',
                 0,
                 VarParsing.VarParsing.multiplicity.singleton,
                 VarParsing.VarParsing.varType.int,
                 "Run for a test or not")
#options.register('pd',
#                 '',
#                 VarParsing.VarParsing.multiplicity.singleton,
#                 VarParsing.VarParsing.varType.string,
#                 "primary dataset name")

options.maxEvents = -1
options.outputFile = 'triggermatch_analyzerMC.root'
options.parseArguments()


if options.test:
    options.inputFiles = 'root://cms-xrd-global.cern.ch///store/user/escalant/HTo2LongLivedTo4mu_MH-125_MFF-20_CTau-130mm_TuneCUETP8M1_13TeV_pythia8/crab_HTo2LongLivedTo4mu_MH-125_MFF-20_CTau-130mm_TuneCUETP8M1_13TeV_pythia8_AODSIM-ReHLT_V37-v1/180128_100504/0000/displacedMuons_RAW2DIGI_L1Reco_RECO_9.root'
    options.maxEvents = -1

process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger = cms.Service("MessageLogger",
    destinations = cms.untracked.vstring('critical', 'cerr'),
    categories   = cms.untracked.vstring('TriggerMatchAnalyzer', 'FwkReport'),
    critical     = cms.untracked.PSet( threshold = cms.untracked.string('INFO') ),
    cerr         = cms.untracked.PSet(
                    threshold = cms.untracked.string('INFO'),
                    FwkReport = cms.untracked.PSet( reportEvery=cms.untracked.int32(1000) ),
                    )
    )

process.options = cms.untracked.PSet(
    wantSummary = cms.untracked.bool(False)
    )

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(options.maxEvents)
    )
import FWCore.PythonUtilities.LumiList as LumiList
#process.source.lumisToProcess = LumiList.LumiList(filename = 'goodList.json').getVLuminosityBlockRange()
#if 'muon' in options.pd.lower():
#    lumiFn = '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/PromptReco/Cert_314472-316271_13TeV_PromptReco_Collisions18_JSON_MuonPhys.txt' 
#else:
#    lumiFn = '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/PromptReco/Cert_314472-316271_13TeV_PromptReco_Collisions18_JSON.txt'
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(options.inputFiles),
    #lumisToProcess = LumiList.LumiList(filename = lumiFn).getVLuminosityBlockRange()
)

process.TFileService = cms.Service("TFileService", 
    fileName = cms.string(options.outputFile),
    closeFileFast = cms.untracked.bool(True)
    )

from DoubleL2Mu.HLTTrigEff.TriggerMatchAnalyzer_cfi import trigmatchana
process.doubleL2dsaMC = trigmatchana.clone(
        processName = cms.untracked.string("*"),
        _trigResults = cms.InputTag("TriggerResults","","DisplacedTriggers"),
        _trigEvent = cms.InputTag("hltTriggerSummaryAOD","","DisplacedTriggers"),
        _trigPath = cms.untracked.string("HLT_DoubleL2Mu25NoVtx_2Cha"),
        _L1TrigFilter = cms.untracked.InputTag("hltL1fL1sDoubleMu155ORTripleMu444L1Filtered0", "", "DisplacedTriggers"),
        _L2TrigFilter = cms.untracked.InputTag("hltL2DoubleMu25NoVertexL2Filtered2Cha", "", "DisplacedTriggers")
        )
process.doubleL2dsaCosmicSeedMC = trigmatchana.clone(
        processName = cms.untracked.string("*"),
        _trigResults = cms.InputTag("TriggerResults","","DisplacedTriggers"),
        _trigEvent = cms.InputTag("hltTriggerSummaryAOD","","DisplacedTriggers"),
        _trigPath = cms.untracked.string("HLT_DoubleL2Mu25NoVtx_2Cha_CosmicSeed"),
        _L1TrigFilter = cms.untracked.InputTag("hltL1fL1sDoubleMu155ORTripleMu444L1Filtered0", "", "DisplacedTriggers"),
        _L2TrigFilter = cms.untracked.InputTag("hltL2fL1sMuORL1f0DoubleL2NoVtx25Q2ChaCosmicSeed", "", "DisplacedTriggers")
        )

process.p = cms.Path(process.doubleL2dsaMC
                   + process.doubleL2dsaCosmicSeedMC
                   )
