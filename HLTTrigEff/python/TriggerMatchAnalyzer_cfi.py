import FWCore.ParameterSet.Config as cms

trigmatchana = cms.EDAnalyzer('TriggerMatchAnalyzer',
    _trigResults = cms.InputTag("TriggerResults","","HLT"),
    _trigEvent = cms.InputTag("hltTriggerSummaryAOD","","HLT"),
    _muons = cms.InputTag("displacedStandAloneMuons"),
    _trigPath = cms.untracked.string('HLT_DoubleL2Mu23NoVtx_2Cha'),
    _L1TrigFilter = cms.untracked.InputTag("hltL1fL1sDoubleMu155ORTripleMu444L1Filtered0", "", "HLT"),
    _L2TrigFilter = cms.untracked.InputTag("hltL2DoubleMu23NoVertexL2Filtered2Cha", "", "HLT"),
    nmuons = cms.untracked.uint32(2),
    processName = cms.untracked.string("HLT"),
    verbose = cms.untracked.bool(False)
)
