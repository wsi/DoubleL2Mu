import FWCore.ParameterSet.Config as cms
import FWCore.ParameterSet.VarParsing as VarParsing

process = cms.Process("USER")

options = VarParsing.VarParsing('analysis')
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger = cms.Service("MessageLogger",
    destinations = cms.untracked.vstring('critical', 'cerr'),
    categories   = cms.untracked.vstring('TriggerMatchAnalyzer', 'FwkReport'),
    critical     = cms.untracked.PSet( threshold = cms.untracked.string('ERROR') ),
    cerr         = cms.untracked.PSet(
                    threshold = cms.untracked.string('INFO'),
                    FwkReport = cms.untracked.PSet( reportEvery=cms.untracked.int32(1000) ),
                    )
    )

process.options = cms.untracked.PSet(
    wantSummary = cms.untracked.bool(False)
    )

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(-1)
    )

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(),
    #fileNames = cms.untracked.vstring('root://cms-xrd-global.cern.ch///store/data/Run2018A/SingleMuon/AOD/PromptReco-v3/000/316/766/00000/B6AFDD9F-8665-E811-8EDC-FA163E79885E.root'),
)

process.TFileService = cms.Service("TFileService", 
    fileName = cms.string('effiana2.root'),
    closeFileFast = cms.untracked.bool(True)
    )

from DoubleL2Mu.HLTTrigEff.EfficiencyAnalyzer2_cfi import effiana2
process.doubleL2dsaMu = effiana2.clone(
    _verbose = cms.untracked.bool(False)
    )
process.doubleL2dsaMuCosmic = process.doubleL2dsaMu.clone(
    _trigPath = cms.untracked.string('HLT_DoubleL2Mu23NoVtx_2Cha_CosmicSeed')
    )

process.p = cms.Path(process.doubleL2dsaMu
                    +process.doubleL2dsaMuCosmic)
