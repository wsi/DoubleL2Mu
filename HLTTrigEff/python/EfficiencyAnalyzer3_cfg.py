import FWCore.ParameterSet.Config as cms
import FWCore.ParameterSet.VarParsing as VarParsing

process = cms.Process("USER")

options = VarParsing.VarParsing('analysis')
process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger = cms.Service("MessageLogger",
    destinations = cms.untracked.vstring('critical', 'cerr'),
    categories   = cms.untracked.vstring('TriggerMatchAnalyzer', 'FwkReport'),
    critical     = cms.untracked.PSet( threshold = cms.untracked.string('ERROR') ),
    cerr         = cms.untracked.PSet(
                    threshold = cms.untracked.string('INFO'),
                    FwkReport = cms.untracked.PSet( reportEvery=cms.untracked.int32(10000) ),
                    )
    )

process.options = cms.untracked.PSet(
    wantSummary = cms.untracked.bool(False),
    # numberOfThreads = cms.untracked.uint32(2)
    )

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(-1)
    )

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(),
    #fileNames = cms.untracked.vstring('root://cms-xrd-global.cern.ch///store/data/Run2018C/SingleMuon/AOD/PromptReco-v2/000/319/659/00000/AE942B4A-058A-E811-90FA-FA163E4F02E8.root'),
)

process.TFileService = cms.Service("TFileService", 
    fileName = cms.string('effiana3.root'),
    closeFileFast = cms.untracked.bool(True)
    )

from DoubleL2Mu.HLTTrigEff.EfficiencyAnalyzer3_cfi import effiana3
process.doubleL2dsaMu = effiana3.clone(
    _verbose = cms.untracked.bool(False)
    )
process.doubleL2dsaMuNoL2Match = process.doubleL2dsaMu.clone(
    _trigPath = cms.untracked.string('HLT_DoubleL2Mu23NoVtx_2Cha_NoL2Matched')
    )
process.doubleL2dsaMuCosmic = process.doubleL2dsaMu.clone(
    _trigPath = cms.untracked.string('HLT_DoubleL2Mu23NoVtx_2Cha_CosmicSeed')
    )
process.doubleL2dsaMuNoL2MatchCosmic = process.doubleL2dsaMu.clone(
    _trigPath = cms.untracked.string('HLT_DoubleL2Mu23NoVtx_2Cha_CosmicSeed_NoL2Matched')
    )


process.p = cms.Path( process.doubleL2dsaMu
                    + process.doubleL2dsaMuNoL2Match 
                    + process.doubleL2dsaMuCosmic
                    + process.doubleL2dsaMuNoL2MatchCosmic 
                     )
