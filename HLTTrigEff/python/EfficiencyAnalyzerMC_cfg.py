import FWCore.ParameterSet.Config as cms
import FWCore.ParameterSet.VarParsing as VarParsing

process = cms.Process("USER")

options = VarParsing.VarParsing('analysis')
options.register('test',
                 0,
                 VarParsing.VarParsing.multiplicity.singleton,
                 VarParsing.VarParsing.varType.int,
                 "Run for a test or not")
options.maxEvents = -1
options.outputFile = 'efficiency_analyzer.root'
options.parseArguments()


if options.test:
    options.inputFiles = 'file:/eos/uscms/store/user/wsi/MET/1C5A560B-434B-E811-8852-FA163ECF234C.root'
    options.maxEvents = -1

process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger = cms.Service("MessageLogger",
    destinations = cms.untracked.vstring('critical', 'cerr'),
    categories   = cms.untracked.vstring('EfficiencyAnalyzer', 'FwkReport'),
    critical     = cms.untracked.PSet( threshold = cms.untracked.string('ERROR') ),
    cerr         = cms.untracked.PSet(
                    threshold = cms.untracked.string('INFO'),
                    FwkReport = cms.untracked.PSet( reportEvery=cms.untracked.int32(1000) ),
                    )
    )

process.options = cms.untracked.PSet(
    wantSummary = cms.untracked.bool(False)
    )

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(options.maxEvents)
    )
import FWCore.PythonUtilities.LumiList as LumiList
#process.source.lumisToProcess = LumiList.LumiList(filename = 'goodList.json').getVLuminosityBlockRange()
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(options.inputFiles),
    #lumisToProcess = LumiList.LumiList(filename = '/afs/cern.ch/work/w/wsi/public/lpcdm/CMSSW_10_1_2_patch2/src/DoubleL2Mu/HLTTrigEff/data/json_DCSONLY.txt').getVLuminosityBlockRange()
)

process.TFileService = cms.Service("TFileService", 
    fileName = cms.string(options.outputFile),
    closeFileFast = cms.untracked.bool(True)
    )

from DoubleL2Mu.HLTTrigEff.EfficiencyAnalyzer_cfi import effiana
# process.load("DoubleL2Mu.HLTTrigEff.EfficiencyAnalyzer_cfi")
process.doubleL2dsaMu = effiana.clone()
process.doubleL2rsaMu = effiana.clone(
    _dsaMuons = cms.InputTag("refittedStandAloneMuons")
)
process.doubleL2MuCosmicSeed = effiana.clone(
    _trigPath = cms.untracked.string('HLT_DoubleL2Mu23NoVtx_2Cha_CosmicSeed_v1')
)

process.L2dsaMu = effiana.clone(
    _trigPath = cms.untracked.string('HLT_L2Mu23NoVtx_2Cha_v1'),
    nmuons = cms.untracked.uint32(1)
)
process.L2rsaMu = effiana.clone(
    _dsaMuons = cms.InputTag("refittedStandAloneMuons"),
    _trigPath = cms.untracked.string('HLT_L2Mu23NoVtx_2Cha_v1'),
    nmuons = cms.untracked.uint32(1)
)
process.L2MuCosmicSeed = effiana.clone(
    _trigPath = cms.untracked.string('HLT_L2Mu23NoVtx_2Cha_CosmicSeed_v1'),
    nmuons = cms.untracked.uint32(1)
)
process.DSTdsaMu = effiana.clone(
    _trigPath = cms.untracked.string('DST_DoubleMu1_noVtx_CaloScouting_v1')
)
process.DSTrsaMu = effiana.clone(
    _trigPath = cms.untracked.string('DST_DoubleMu1_noVtx_CaloScouting_v1'),
    _dsaMuons = cms.InputTag("refittedStandAloneMuons")
)

# MC
process.MCL2dsaMu = effiana.clone(
    _trigPath = cms.untracked.string('HLT_DoubleL2Mu25NoVtx_2Cha_v4'),
    _trigResults = cms.InputTag("TriggerResults","","DisplacedTriggers")
)
process.MCL2dsaMuCosmicSeed = effiana.clone(
    _trigPath = cms.untracked.string('HLT_DoubleL2Mu25NoVtx_2Cha_CosmicSeed_v4'),
    _trigResults = cms.InputTag("TriggerResults","","DisplacedTriggers")
)

process.MCL2rsaMu = effiana.clone(
    _dsaMuons = cms.InputTag("refittedStandAloneMuons"),
    _trigPath = cms.untracked.string('HLT_DoubleL2Mu25NoVtx_2Cha_v4'),
    _trigResults = cms.InputTag("TriggerResults","","DisplacedTriggers")
)
process.MCL2dsaMuVtx = effiana.clone(
    _trigPath = cms.untracked.string('HLT_DoubleL2Mu25_v2'),
    _trigResults = cms.InputTag("TriggerResults","","DisplacedTriggers")
)
process.MCDSTdsaMu = effiana.clone(
    _trigPath = cms.untracked.string('DST_DoubleMu3_noVtx_CaloScouting_v2')
)


from DoubleL2Mu.HLTTrigEff.GenAnalyzer_cfi import genana
process.MCL2dsaGenMu = genana.clone()
#process.p = cms.Path(process.doubleL2dsaMu
#                   + process.doubleL2rsaMu
#                   + process.doubleL2MuCosmicSeed
#                   + process.L2dsaMu
#                   + process.L2rsaMu
#                   + process.L2MuCosmicSeed
#                   + process.DSTdsaMu
#                   + process.DSTrsaMu)
process.p = cms.Path(process.MCL2dsaMu
                    +process.MCL2rsaMu
                    +process.MCL2dsaMuCosmicSeed
                    +process.MCL2dsaMuVtx
                    +process.MCDSTdsaMu
                    +process.MCL2dsaGenMu)
