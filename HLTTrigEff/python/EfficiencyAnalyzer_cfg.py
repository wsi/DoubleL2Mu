import FWCore.ParameterSet.Config as cms
import FWCore.ParameterSet.VarParsing as VarParsing

process = cms.Process("USER")

options = VarParsing.VarParsing('analysis')
options.register('test',
                 0,
                 VarParsing.VarParsing.multiplicity.singleton,
                 VarParsing.VarParsing.varType.int,
                 "Run for a test or not")
options.register('pd',
                 '',
                 VarParsing.VarParsing.multiplicity.singleton,
                 VarParsing.VarParsing.varType.string,
                 "primary dataset name")
options.maxEvents = -1
options.outputFile = 'efficiency_analyzer.root'
options.parseArguments()


if options.test:
    options.inputFiles = 'file:/eos/uscms/store/user/wsi/MET/1C5A560B-434B-E811-8852-FA163ECF234C.root'
    options.maxEvents = -1

process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger = cms.Service("MessageLogger",
    destinations = cms.untracked.vstring('critical', 'cerr'),
    categories   = cms.untracked.vstring('EfficiencyAnalyzer', 'FwkReport'),
    critical     = cms.untracked.PSet( threshold = cms.untracked.string('ERROR') ),
    cerr         = cms.untracked.PSet(
                    threshold = cms.untracked.string('INFO'),
                    FwkReport = cms.untracked.PSet( reportEvery=cms.untracked.int32(1000) ),
                    )
    )

process.options = cms.untracked.PSet(
    wantSummary = cms.untracked.bool(False)
    )

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(options.maxEvents)
    )
import FWCore.PythonUtilities.LumiList as LumiList
#process.source.lumisToProcess = LumiList.LumiList(filename = 'goodList.json').getVLuminosityBlockRange()
if 'muon' in options.pd.lower():
    #lumiFn = '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/PromptReco/Cert_314472-315810_13TeV_PromptReco_Collisions18_JSON_MuonPhys.txt' 
    lumiFn = '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/PromptReco/Cert_314472-316271_13TeV_PromptReco_Collisions18_JSON_MuonPhys.txt'
else:
    #lumiFn = '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/PromptReco/Cert_314472-315801_13TeV_PromptReco_Collisions18_JSON.txt'
    lumiFn = '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/PromptReco/Cert_314472-316271_13TeV_PromptReco_Collisions18_JSON.txt'
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(options.inputFiles),
    lumisToProcess = LumiList.LumiList(filename = lumiFn).getVLuminosityBlockRange()
)

process.TFileService = cms.Service("TFileService", 
    fileName = cms.string(options.outputFile),
    closeFileFast = cms.untracked.bool(True)
    )

from DoubleL2Mu.HLTTrigEff.EfficiencyAnalyzer_cfi import effiana
process.doubleL2dsaMu = effiana.clone()
process.doubleL2rsaMu = effiana.clone(
    _dsaMuons = cms.InputTag("refittedStandAloneMuons")
)
process.doubleL2MuCosmicSeed = effiana.clone(
    _trigPath = cms.untracked.string('HLT_DoubleL2Mu23NoVtx_2Cha_CosmicSeed_v'),
    _trigFilter = cms.untracked.InputTag("hltL2fL1sMuORL1f0DoubleL2NoVtx23Q2ChaCosmicSeed","","HLT")
)

process.L2dsaMu = effiana.clone(
    _trigPath = cms.untracked.string('HLT_L2Mu23NoVtx_2Cha_v'),
    _trigFilter = cms.untracked.InputTag("hltL2fL1sMuORL1f0L2NoVtx23Q2Cha","","HLT"),
    nmuons = cms.untracked.uint32(1)
)
process.L2rsaMu = effiana.clone(
    _dsaMuons = cms.InputTag("refittedStandAloneMuons"),
    _trigPath = cms.untracked.string('HLT_L2Mu23NoVtx_2Cha_v'),
    _trigFilter = cms.untracked.InputTag("hltL2fL1sMuORL1f0L2NoVtx23Q2Cha","","HLT"),
    nmuons = cms.untracked.uint32(1)
)
process.L2MuCosmicSeed = effiana.clone(
    _trigPath = cms.untracked.string('HLT_L2Mu23NoVtx_2Cha_CosmicSeed_v'),
    _trigFilter = cms.untracked.InputTag("hltL2fL1sMuORL1f0L2NoVtx23Q2ChaCosmicSeed","","HLT"),
    nmuons = cms.untracked.uint32(1)
)
process.DSTdsaMu = effiana.clone(
    _trigPath = cms.untracked.string('DST_DoubleMu1_noVtx_CaloScouting_v'),
    _trigFilter = cms.untracked.InputTag("hltDoubleMu1L3FilteredNoVtx","","HLT")
)
process.DSTrsaMu = effiana.clone(
    _trigPath = cms.untracked.string('DST_DoubleMu1_noVtx_CaloScouting_v'),
    _trigFilter = cms.untracked.InputTag("hltDoubleMu1L3FilteredNoVtx","","HLT"),
    _dsaMuons = cms.InputTag("refittedStandAloneMuons")
)

process.p = cms.Path(process.doubleL2dsaMu
                   + process.doubleL2rsaMu
                   + process.doubleL2MuCosmicSeed
                   + process.L2dsaMu
                   + process.L2rsaMu
                   + process.L2MuCosmicSeed
                   + process.DSTdsaMu
                   + process.DSTrsaMu)
