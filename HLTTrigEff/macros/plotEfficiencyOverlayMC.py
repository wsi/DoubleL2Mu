#!/usr/bin/env python

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.gStyle.SetLegendFont(42)

#inputFile = '../test/MET.root'
inputFile = '../test/MC_125_20_13mm.root'
outputFile = '../test/efficiency_analyzer_MC.pdf'
triggerPath = 'L2MuCosmicSeed'
requireMassCut = False 

def get_effi(inputFile, triggerPath, requireMassCut):
    denom = ROOT.TH1D('denom', 'Effciency denominator', 30, 0, 100)
    numer = ROOT.TH1D('numer', 'Effciency numerator'  , 30, 0, 100)
    
    inf = ROOT.TFile(inputFile)
    source = inf.Get(triggerPath+'/displacedStandAloneMuons')
    for mu in source:
        if requireMassCut:
            if mu.passMassCut: denom.Fill(mu.pt)
            if mu.passMassCut and mu.passTrigger: numer.Fill(mu.pt)
        else:
            denom.Fill(mu.pt)
            if mu.passTrigger: numer.Fill(mu.pt)
    
    effi = ROOT.TGraphAsymmErrors()
    effi.BayesDivide(numer, denom)
    effi.SetMaximum(1.05)
    effi.SetMinimum(0)
    effi.GetXaxis().SetTitle('Subleading #mu p_{T} [GeV]')
    effi.GetXaxis().SetNdivisions(16)
    #effi.GetXaxis().SetLabelSize(0.02)
    effi.GetYaxis().SetTitle('Efficiency')
    effi.GetYaxis().SetNdivisions(16)
    
    effi.SetMarkerColorAlpha(ROOT.kBlue, 0.8)
    effi.SetMarkerStyle(ROOT.kFullCircle)
    effi.SetLineColorAlpha(ROOT.kBlue, 0.8)
    return effi

c = ROOT.TCanvas()
c.SetGrid()
effi0 = get_effi(inputFile, 'MCL2dsaMu', requireMassCut)
effi0.Draw('ap')
effi1 = get_effi(inputFile, 'MCL2rsaMu', requireMassCut)
effi1.SetMarkerColorAlpha(ROOT.kRed, 0.8)
effi1.SetLineColorAlpha(ROOT.kRed, 0.8)
effi1.Draw('p')
effi2 = get_effi(inputFile, 'MCL2dsaMuCosmicSeed', requireMassCut)
effi2.SetMarkerColorAlpha(ROOT.kGreen, 0.8)
effi2.SetLineColorAlpha(ROOT.kGreen, 0.8)
effi2.Draw('p')

leg = ROOT.TLegend(0.4, 0.1857, 0.85, 0.333)
leg.AddEntry(effi0, 'DoubleL2Mu25NoVtx_2Cha dSA', 'lep')
leg.AddEntry(effi1, 'DoubleL2Mu25NoVtx_2Cha rSA', 'lep')
leg.AddEntry(effi2, 'DoubleL2Mu25NoVtx_2Cha_CosmicSeed dSA', 'lep')
leg.Draw()

Label = ROOT.TPaveText(0.11, 0.9, 0.35, 0.94, 'NDC')
Label.AddText('MC M_{H}125-M_{#Chi}20-c#tau13mm')
Label.SetFillColor(0)
Label.SetLineColor(0)
Label.SetFillStyle(0)
Label.SetTextFont(42)
Label.SetTextAlign(11)
Label.SetBorderSize(0)
Label.Draw()
c.Print(outputFile)
