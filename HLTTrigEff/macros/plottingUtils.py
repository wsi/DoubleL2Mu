#!/usr/bin/env python
import ROOT
import numpy as np

def getEffi2D(inputFile, triggerPath, minPt=0, massRequired=False):
    '''inputFile: a ROOT.TFile()
       triggerPath: name string of TDirectory of inputFile
       minPt: asking for minimum pT
       massRequired: if dimuon mass in Z required

       return ROOT.TEfficiency()
    '''

    denom = ROOT.TH2D('denom', 'Effciency denominator', 5, 0, 100, 5, 0, 100)
    numer = ROOT.TH2D('numer', 'Effciency numerator'  , 5, 0, 100, 5, 0, 100)

    t1 = inputFile.Get(triggerPath+'/leadingMu')
    t2 = inputFile.Get(triggerPath+'/subleadingMu')
    s1 = [{'pt': m.pt, 'dxy': m.dxy, 'triggered': m.passTrigger, 'inZ': m.passMassCut} for m in t1]
    s2 = [{'pt': m.pt, 'dxy': m.dxy, 'triggered': m.passTrigger, 'inZ': m.passMassCut} for m in t2]

    for l, sl in zip(s1, s2):
        if l['pt'] < minPt or sl['pt'] < minPt: continue
        if massRequired:
            if not l['inZ'] or not sl['inZ']: continue

        denom.Fill(sl['dxy'], l['dxy'])
        if l['triggered'] and sl['triggered']:
            numer.Fill(sl['dxy'], l['dxy'])

    effi = ROOT.TEfficiency(numer, denom)
    effi.SetTitle(';Subleading #mu dxy [cm];Leading #mu dxy [cm];Efficiency')
    effi.SetMarkerSize(1.5)
    effi.SetMarkerColor(0)

    return effi

def getContentFrom2DTEffi(effi):
    '''effi: a 2D ROOT.TEfficiency()

       return: a dict with key (xbin, ybin), val (efficiency, average error)
    '''

    kx = effi.GetTotalHistogram().GetNbinsX()
    ky = effi.GetTotalHistogram().GetNbinsY()

    data = {}
    for y in xrange(ky, 0, -1):
        for x in xrange(1, kx+1):
            gb = effi.GetGlobalBin(x, y)
            data[(x,y)] = (effi.GetEfficiency(gb),
                           (effi.GetEfficiencyErrorUp(gb)+effi.GetEfficiencyErrorLow(gb))/2)

    return data

def extract1DTEff(effi):
    c = ROOT.TCanvas('c','', 600,500)
    effi.Draw('ap')
    ROOT.gPad.Update()

    asymm = effi.GetPaintedGraph()
    tot = effi.GetTotalHistogram()
    nBins = tot.GetNbinsX()

    xx = np.array([tot.GetXaxis().GetBinCenter(i) for i in xrange(1, nBins)])
    yy = np.array([effi.GetEfficiency(effi.GetGlobalBin(i)) for i in xrange(1, nBins)])
    xeh = [asymm.GetErrorXhigh(i) for i in xrange(1, nBins)]
    xel = [asymm.GetErrorXlow(i) for i in xrange(1, nBins)]
    xe = np.array([xel, xeh])
    yeh = [asymm.GetErrorYhigh(i) for i in xrange(1, nBins)]
    yel = [asymm.GetErrorYlow(i) for i in xrange(1, nBins)]
    ye = np.array([yel, yeh])

    return {'x':xx, 'y':yy, 'xerr':xe, 'yerr':ye}

def convert2DTEffiToTH2D(effi, title='', zrange=(0,1)):
    ''' effi: ROOT.TEfficiency()
        title: TH2D title
        zrange: self-explanatory

        return a TH2D with correct error calculated by TEfficiency
    '''

    kx = effi.GetTotalHistogram().GetNbinsX()
    ky = effi.GetTotalHistogram().GetNbinsY()

    xmin = effi.GetTotalHistogram().GetXaxis().GetXmin()
    xmax = effi.GetTotalHistogram().GetXaxis().GetXmax()
    ymin = effi.GetTotalHistogram().GetYaxis().GetXmin()
    ymax = effi.GetTotalHistogram().GetYaxis().GetXmax()

    h = ROOT.TH2D('h', title, kx, xmin, xmax, ky, ymin, ymax)
    data = getContentFrom2DTEffi(effi)
    for k in data:
        x, y = k
        val, err = data[k]
        h.SetBinContent(x,y,val)
        h.SetBinError(x,y,err)

    h.SetMarkerSize(1.5)
    h.SetMarkerColor(0)
    if zrange is not None:
        zmin, zmax = zrange
        h.GetZaxis().SetRangeUser(zmin, zmax)
    return h

#from root_numpy import tree2array
#def getEffi2DWithNMuonRestricted(inputFile, triggerPath, minPt=0,
#        massRequired=False, nMuonRestrict=2):
#    '''inputFile: a ROOT.TFile()
#       triggerPath: name string of TDirectory of inputFile
#       minPt: asking for minimum pT
#       massRequired: if dimuon mass in Z required
#
#       return ROOT.TEfficiency()
#    '''
#
#    denom = ROOT.TH2D('denom', 'Effciency denominator', 5, 0, 100, 5, 0, 100)
#    numer = ROOT.TH2D('numer', 'Effciency numerator'  , 5, 0, 100, 5, 0, 100)
#
#    t0 = inputFile.Get(triggerPath+'/event')
#    t1 = inputFile.Get(triggerPath+'/leadingMu')
#    t2 = inputFile.Get(triggerPath+'/subleadingMu')
#
#    eventArray = tree2array(t0)
#    leadMuArray = tree2array(t1)
#    subleadMuArray = tree2array(t2)
#
#    for i in xrange(eventArray['NMuon'].size):
#        if leadMuArray['pt'][i] < minPt or subleadMuArray['pt'][i] < minPt: continue
#        if eventArray['NMuon'][i] != nMuonRestrict: continue
#        if massRequired:
#            if not leadMuArray['passMassCut'][i] or not subleadMuArray['passMassCut'][i]: continue
#        denom.Fill(subleadMuArray['dxy'][i], leadMuArray['dxy'][i])
#        if subleadMuArray['passTrigger'][i] and leadMuArray['passTrigger'][i]:
#            numer.Fill(subleadMuArray['dxy'][i], leadMuArray['dxy'][i])
#
#    effi = ROOT.TEfficiency(numer, denom)
#    effi.SetTitle(';Subleading #mu dxy [cm];Leading #mu dxy [cm];Efficiency')
#    effi.SetMarkerSize(1.5)
#    effi.SetMarkerColor(0)
#
#    return effi

def getEffi2DWithNMuonRestricted(inputFile, triggerPath, minPt=0, massRequired=False,
        triggerMatch=False, nMuonRestrict=2):
    '''inputFile: a ROOT.TFile()
       triggerPath: name string of TDirectory of inputFile
       minPt: asking for minimum pT
       massRequired: if dimuon mass in Z required

       return ROOT.TEfficiency()
    '''

    denom = ROOT.TH2D('denom', 'Effciency denominator', 5, 0, 100, 5, 0, 100)
    numer = ROOT.TH2D('numer', 'Effciency numerator'  , 5, 0, 100, 5, 0, 100)

    t0 = inputFile.Get(triggerPath+'/event')
    t1 = inputFile.Get(triggerPath+'/leadingMu')
    t2 = inputFile.Get(triggerPath+'/subleadingMu')
    s0 = [{'nMuon': e.NMuon} for e in t0]
    s1 = [{'pt': m.pt, 'vxy': m.vxy, 'triggered': m.passTrigger, 'triggerMatch': m.triggerMatch, 'inZ': m.passMassCut} for m in t1]
    s2 = [{'pt': m.pt, 'vxy': m.vxy, 'triggered': m.passTrigger, 'triggerMatch': m.triggerMatch, 'inZ': m.passMassCut} for m in t2]

    for e, l, sl in zip(s0, s1, s2):
        if l['pt'] < minPt or sl['pt'] < minPt: continue
        if e['nMuon'] != nMuonRestrict: continue
        if massRequired:
            if not l['inZ'] or not sl['inZ']: continue
        
        denom.Fill(sl['vxy'], l['vxy'])

        if l['triggered'] and sl['triggered']:
            if triggerMatch:
                if not l['triggerMatch'] or not sl['triggerMatch']: continue
            numer.Fill(sl['vxy'], l['vxy'])

    effi = ROOT.TEfficiency(numer, denom)
    effi.SetTitle(';Subleading #mu vxy [cm];Leading #mu vxy [cm];Efficiency')
    effi.SetMarkerSize(1.5)
    effi.SetMarkerColor(0)

    return effi

