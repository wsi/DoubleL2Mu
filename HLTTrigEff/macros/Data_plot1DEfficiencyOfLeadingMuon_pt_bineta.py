#!/usr/bin/env python

# make 1D plot of leading muon of single muon path
# efficiency as a function of pT in bins

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.gStyle.SetLegendFont(42)
ROOT.gStyle.SetLabelSize(0.02, "xyz")

inputDataName = 'MET.root'
infn = '../test/{0}'.format(inputDataName)
inf = ROOT.TFile(infn)

trigTag = 'L2rsaMu'
#dsa = inf.Get('L2dsaMu/leadingMu')
#dsa = inf.Get('L2rsaMu/leadingMu')
dsa = inf.Get('%s/leadingMu' % trigTag)

def get_efficiency(tree, minPt=0, maxNormChi2=10, etaCut=lambda x: abs(x)>0):
    denom = ROOT.TH1D('denom', 'Effciency denominator', 30, 0, 100)
    numer = ROOT.TH1D('numer', 'Effciency numerator', 30, 0, 100)
    
    for mu in tree:
        if mu.pt < minPt: continue
        if mu.nChi2 > maxNormChi2: continue
        if not etaCut(mu.dxy): continue
        val = mu.pt
        denom.Fill(val)
        if mu.passTrigger and mu.triggerMatch:
            numer.Fill(val)
    
    effi = ROOT.TGraphAsymmErrors()
    effi.BayesDivide(numer, denom)
    effi.SetMaximum(1.05)
    effi.SetMinimum(0)
    effi.GetXaxis().SetTitle('Leading #mu p_{T} [GeV]')
    #effi.GetXaxis().SetNdivisions(32)
    effi.GetYaxis().SetTitle('Efficiency')
    #effi.GetYaxis().SetNdivisions(16)
    
    effi.SetMarkerStyle(ROOT.kFullCircle)
    return effi

import collections
EfficiencyCollection = collections.OrderedDict()
EfficiencyCollection[(0,0.9)] =            get_efficiency(dsa, etaCut=lambda x: abs(x)<=0.9 and abs(x)>=0)
EfficiencyCollection[(0.9,1.2)]=           get_efficiency(dsa, etaCut=lambda x: abs(x)<=1.2 and abs(x)>0.9)
EfficiencyCollection[(1.2,2.0)]=           get_efficiency(dsa, etaCut=lambda x: abs(x)<=2.0 and abs(x)>1.2)
#cosmicEffi = get_efficiency(cosmic, minPt=35, maxNormChi2=2)

c = ROOT.TCanvas('c','',500,500)
c.SetGrid()

colors = [ROOT.kOrange, ROOT.kRed, ROOT.kGreen, ROOT.kMagenta, ROOT.kBlue]
for k, e in EfficiencyCollection.iteritems():
    index = EfficiencyCollection.keys().index(k)
    e.SetMarkerColorAlpha(colors[index], 1.)
    e.SetLineColorAlpha(colors[index], 1.)
    if index == 0:
        e.Draw('ap')
    else:
        e.Draw('p')

leg = ROOT.TLegend(0.4, 0.79, 0.88, 0.89)
for k, e in EfficiencyCollection.iteritems():
    text = 'L2Mu23NoVtx_2Cha rSA |#eta|:({0}, {1})'.format(k[0], k[1])
    leg.AddEntry(e, text, 'lep')
leg.Draw()

Label = ROOT.TPaveText(0.08, 0.9, 0.55, 0.95, 'NDC')
Label.AddText('MET 2018A PromptReco')
Label.SetFillColor(0)
Label.SetLineColor(0)
Label.SetFillStyle(0)
Label.SetTextFont(62)
Label.SetTextAlign(13)
Label.SetTextSize(0.025)
Label.SetBorderSize(0)
Label.Draw()

Label1 = ROOT.TPaveText(0.58, 0.9, 0.92, 0.95, 'NDC')
Label1.AddText('7.93fb^{-1} (Run314472-316271)')
Label1.SetFillColor(0)
Label1.SetLineColor(0)
Label1.SetFillStyle(0)
Label1.SetTextFont(42)
Label1.SetTextAlign(33)
Label1.SetTextSize(0.023)
Label1.SetBorderSize(0)
Label1.Draw()

outputFolder = '/eos/user/w/wsi/www/plots/L2Mu/{0}'.format(inputDataName.rstrip('.root'))
import os
if not os.path.isdir(outputFolder): os.mkdir(outputFolder)
outfn = 'leadingMuEfficiencyOfpT_pt35_etaBinned_%s.pdf' % trigTag
outfn = os.path.join(outputFolder, outfn)
c.Print(outfn)
