#!/usr/bin/env python

pd = 'MET'
#pd = 'SingleMuon'

inf = '../data/{0}_Run2018A_PromptReco_AOD.list'.format(pd)
splitEvery = 40
outf = '../data/{0}/{0}_Run2018A_PromptReco_AOD_{1}.list'

import os
if not os.path.isdir('../data/{0}'.format(pd)):
    os.mkdir('../data/{0}'.format(pd))

#prefix = 'root://xrootd-cms.infn.it/'
prefix = 'root://cms-xrd-global.cern.ch/'
datalist = open(inf).readlines()
numOfFiles = len(datalist)
for i, begin in enumerate(xrange(0, numOfFiles, splitEvery)):
    if begin+splitEvery < numOfFiles:
        with open(outf.format(pd, i), 'w') as of:
            for d in datalist[begin:begin+splitEvery]:
                of.write(prefix+d)
    else:
        with open(outf.format(pd, i), 'w') as of:
            for d in datalist[begin:-1]:
                of.write(prefix+d)
