#!/usr/bin/env python

# make 1D plot of leading muon of single muon path
# efficiency as a function of |dxy|/dxyError

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.gStyle.SetLegendFont(42)
ROOT.gStyle.SetLabelSize(0.02, "xyz")

inputDataName = 'MET.root'
infn = '../test/{0}'.format(inputDataName)
inf = ROOT.TFile(infn)

dsa = inf.Get('L2dsaMu/leadingMu')
cosmic = inf.Get('L2MuCosmicSeed/leadingMu')

def get_efficiency(tree, minPt=0, maxNormChi2=10):
    denom = ROOT.TH1D('denom', 'Effciency denominator', 20, 0, 20)
    numer = ROOT.TH1D('numer', 'Effciency numerator', 20, 0, 20)
    
    for mu in tree:
        if mu.pt < minPt: continue
        if mu.nChi2 > maxNormChi2: continue
        val = abs(mu.dxy)/mu.dxyError
        denom.Fill(val)
        if mu.passTrigger:
            numer.Fill(val)
    
    effi = ROOT.TGraphAsymmErrors()
    effi.BayesDivide(numer, denom)
    effi.SetMaximum(1.05)
    effi.SetMinimum(0)
    effi.GetXaxis().SetTitle('Leading #mu |dxy|/#sigma(dxy)')
    #effi.GetXaxis().SetNdivisions(32)
    effi.GetYaxis().SetTitle('Efficiency')
    #effi.GetYaxis().SetNdivisions(16)
    
    effi.SetMarkerStyle(ROOT.kFullCircle)
    return effi

dsaEffi =    get_efficiency(dsa,    minPt=35, maxNormChi2=2)
cosmicEffi = get_efficiency(cosmic, minPt=35, maxNormChi2=2)

c = ROOT.TCanvas('c','',500,500)
c.SetGrid()

dsaEffi.SetMarkerColorAlpha(ROOT.kBlue, 0.8)
dsaEffi.SetLineColorAlpha(ROOT.kBlue, 0.8)
dsaEffi.Draw('ap')
cosmicEffi.SetMarkerColorAlpha(ROOT.kRed, 0.8)
cosmicEffi.SetLineColorAlpha(ROOT.kRed, 0.8)
cosmicEffi.Draw('p')

leg = ROOT.TLegend(0.4, 0.78, 0.85, 0.88)
leg.AddEntry(dsaEffi, 'L2Mu23NoVtx_2Cha dSA', 'lep')
leg.AddEntry(cosmicEffi, 'L2Mu23NoVtx_2Cha_CosmicSeed dSA', 'lep')
# leg.SetTextSize(0.022)
leg.Draw()

Label = ROOT.TPaveText(0.08, 0.9, 0.55, 0.95, 'NDC')
Label.AddText('MET 2018A PromptReco')
Label.SetFillColor(0)
Label.SetLineColor(0)
Label.SetFillStyle(0)
Label.SetTextFont(62)
Label.SetTextAlign(13)
Label.SetTextSize(0.025)
Label.SetBorderSize(0)
Label.Draw()

Label1 = ROOT.TPaveText(0.58, 0.9, 0.92, 0.95, 'NDC')
Label1.AddText('4.051fb^{-1} (Run314472-315721)')
Label1.SetFillColor(0)
Label1.SetLineColor(0)
Label1.SetFillStyle(0)
Label1.SetTextFont(42)
Label1.SetTextAlign(33)
Label1.SetTextSize(0.023)
Label1.SetBorderSize(0)
Label1.Draw()

outputFolder = '/eos/user/w/wsi/www/plots/L2Mu/{0}'.format(inputDataName.rstrip('.root'))
import os
if not os.path.isdir(outputFolder): os.mkdir(outputFolder)
outfn = 'leadingMuEfficiencyOfDxyOverDxyError_pt35_nChi2.pdf'
outfn = os.path.join(outputFolder, outfn)
c.Print(outfn)
