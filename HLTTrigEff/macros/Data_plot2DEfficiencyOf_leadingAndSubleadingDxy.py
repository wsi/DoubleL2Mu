#!/usr/bin/env python

# making plots of 2D trigger efficiency
# as a funciton of
# X: subleading muon dxy
# Y: leading muon dxy

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.gStyle.SetLegendFont(42)
ROOT.gStyle.SetLabelSize(0.02, "xyz")
ROOT.gStyle.SetPaintTextFormat( "4.3f" )
ROOT.gStyle.SetPalette(112)

import sys
#triggerTags = ['doubleL2dsaMu', 'doubleL2rsaMu', 'doubleL2MuCosmicSeed',
#        'DSTdsaMu', 'DSTrsaMu']
triggerTags = [ 'DSTdsaMu']

#try:
#    triggerTag = sys.argv[1]
#except IndexError:
#    errMsg = 'Need 1 argument. Available: {0}'.format(str(triggerTags))
#    sys.exit(errMsg)
#if triggerTag not in triggerTags:
#    sys.exit('"{0}" not exist!\nAvailable: {1}'.format(triggerTag, str(triggerTags)))

inputDataName = 'SingleMuon.root'
infn = '../test/{0}'.format(inputDataName)
inf = ROOT.TFile(infn)

from plottingUtils import *


for triggerTag in triggerTags:
    c = ROOT.TCanvas('c{0}'.format(triggerTag),'',500,500)
    
    eff = getEffi2DWithNMuonRestricted(inf, triggerTag, minPt=35,
            massRequired=True, nMuonRestrict=2)
    
    htitle = ';Subleading #mu dxy [cm];Leading #mu dxy [cm];Efficiency'
    h = convert2DTEffiToTH2D(eff, title=htitle)
    
    h.Draw('colz text E')
    ROOT.gPad.Update()
    h.GetListOfFunctions().FindObject('palette').SetX2NDC(0.92)
    
    Label = ROOT.TPaveText(0.08, 0.9, 0.55, 0.95, 'NDC')
    Label.AddText('SingleMuon 2018A PromptReco')
    Label.SetFillColor(0)
    Label.SetLineColor(0)
    Label.SetFillStyle(0)
    Label.SetTextFont(62)
    Label.SetTextAlign(13)
    Label.SetTextSize(0.025)
    Label.SetBorderSize(0)
    Label.Draw()
    
    Label1 = ROOT.TPaveText(0.58, 0.9, 0.92, 0.95, 'NDC')
    Label1.AddText('4.051fb^{-1} (Run314472-315721)')
    Label1.SetFillColor(0)
    Label1.SetLineColor(0)
    Label1.SetFillStyle(0)
    Label1.SetTextFont(42)
    Label1.SetTextAlign(33)
    Label1.SetTextSize(0.023)
    Label1.SetBorderSize(0)
    Label1.Draw()

    #outputFolder = '../test/triggerEfficiencyResults'
    outputFolder = '/eos/user/w/wsi/www/plots/L2Mu/{0}'.format(inputDataName.rstrip('.root'))
    import os
    if not os.path.isdir(outputFolder): os.mkdir(outputFolder)
    outfn = 'leadingAndSubleadingDxy_{0}.pdf'.format(triggerTag)
    outfn = os.path.join(outputFolder, outfn)
    c.Print(outfn)
