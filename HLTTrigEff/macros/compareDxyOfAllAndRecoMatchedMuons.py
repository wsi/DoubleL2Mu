#!/usr/bin/env python

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.gStyle.SetLegendFont(42)
ROOT.gStyle.SetLabelSize(0.02, "xyz")

inputDataName = 'MET_trigAll.root'
infn = '../test/%s' % inputDataName
inf = ROOT.TFile(infn)

#tree = inf.Get('L2Mu23dSA/leadingMu')
tree = inf.Get('L2Mu23dGM/leadingMu')

c = ROOT.TCanvas('c', '', 500, 500)
c.cd()
ROOT.gPad.SetLogy()

conditions = [
        (lambda m: True, 'All displaced global Muons'),
        (lambda m: m.recoMuMatch, 'dGM Muons matched with globalMuon'),
        (lambda m: m.recoMuMatch and m.pt>35, 'matched with globalMuon + pT>35'),
        (lambda m: m.recoMuMatch and m.pt>35 and m.triggerMatch, 'matched with globalMuon + matched with trigger object + pT>35'),
        (lambda m: m.recoMuMatch and m.pt>35 and m.triggerMatch and m.innerxy<100, 'matched with globalMuon + matched with trigger object + pT>35 + innerxy<100')
        ]

hs = []
for i in xrange(len(conditions)):
    hs.append(ROOT.TH1F('h%d'%i, ';leading #mu |dxy| [cm];Num. Of Muons', 10, 0, 50))

for mu in tree:
    for cond, h in zip(conditions, hs):
        if cond[0](mu): h.Fill(abs(mu.dxy))

colors = [ROOT.kBlue, ROOT.kRed, ROOT.kGreen, ROOT.kCyan, ROOT.kOrange]
for h, cl in zip(hs, colors):
    h.SetLineColor(cl)
    h.SetMinimum(1.)

for i, h in enumerate(hs):
    h.Draw('HIST' if i==0 else 'HISTSAME')

leg = ROOT.TLegend(0.5, 0.7, 0.88, 0.88)
for cond, h in zip(conditions, hs):
    leg.AddEntry(h, cond[1], 'l')
leg.Draw()

Label = ROOT.TPaveText(0.08, 0.9, 0.55, 0.95, 'NDC')
Label.AddText('MET 2018A PromptReco')
Label.SetFillColor(0)
Label.SetLineColor(0)
Label.SetFillStyle(0)
Label.SetTextFont(62)
Label.SetTextAlign(13)
Label.SetTextSize(0.025)
Label.SetBorderSize(0)
Label.Draw()

Label1 = ROOT.TPaveText(0.58, 0.9, 0.92, 0.95, 'NDC')
Label1.AddText('7.93fb^{-1} (Run314472-316271)')
Label1.SetFillColor(0)
Label1.SetLineColor(0)
Label1.SetFillStyle(0)
Label1.SetTextFont(42)
Label1.SetTextAlign(33)
Label1.SetTextSize(0.023)
Label1.SetBorderSize(0)
Label1.Draw()


c.Print('dxyOfAllAndRecoMatchedMuons_dGM.pdf')
