#! /usr/bin/env python

# dump event info for edmPickEvents.py

import ROOT
ROOT.gROOT.SetBatch(True)

infn = '../test/MET.root'
inf = ROOT.TFile(infn)

triggerPath = 'L2dsaMu' # L2MuCosimcSeed
muons = inf.Get(triggerPath + '/leadingMu')
events = inf.Get(triggerPath + '/event')

from root_numpy import tree2array
muonsArray = tree2array(muons)
eventsArray = tree2array(events)

for i in xrange(muonsArray['dxy'].size):
    if muonsArray['dxy'][i] < 10: continue
    if not muonsArray['passTrigger'][i]: continue
    print '{0}:{1}:{2}'.format(eventsArray['RunNumber'][i],
            eventsArray['LumiBlock'][i],
            eventsArray['EventNumber'][i])
