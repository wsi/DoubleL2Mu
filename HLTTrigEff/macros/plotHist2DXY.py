#1/usr/bin/env python

import ROOT

def makingPlot(trigTag):
    inputDataName = 'MET.root'
    infn = '../test/{0}'.format(inputDataName)

    inf = ROOT.TFile(infn)
    tr = inf.Get('{0}/leadingMu'.format(trigTag))

    from root_numpy import tree2array
    ar = tree2array(tr)
    denomDef = ar['pt']>35
    numerDef = denomDef & (ar['passTrigger']==1) & (ar['triggerMatch']==1)

    import matplotlib as mpl
    import matplotlib.pyplot as plt
    import matplotlib.ticker as ticker
    from matplotlib.colors import LogNorm
    mpl.rcParams['agg.path.chunksize'] = 10000
    plt.style.use('seaborn-paper')
    plt.set_cmap('magma_r')
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.xaxis.set_major_locator(ticker.MultipleLocator(100))
    ax.yaxis.set_major_locator(ticker.MultipleLocator(50))
    ax.grid()

    #var_x = 'vz'#'innerz'
    #var_y = 'vxy'#'innerxy'
    var_x = 'innerz'
    var_y = 'innerxy'

    counts, xedges, yedges, im = ax.hist2d(ar[numerDef][var_x], ar[numerDef][var_y], bins=100, norm=LogNorm())
    plt.colorbar(im, ax=ax)

    plt.xlabel('leading mu {0} [cm]'.format(var_x))
    plt.ylabel('leading mu {0} [cm]'.format(var_y))
    #plt.xlim((0, 700))
    plt.ylim((0, 700))

    outputFolder = '/eos/user/w/wsi/www/plots/L2Mu/{0}'.format(inputDataName.rstrip('.root'))
    import os
    if not os.path.isdir(outputFolder): os.mkdir(outputFolder)
    outfn = 'hist2d_{1}{2}_{0}.pdf'.format(trigTag, var_y, var_x)
    outfn = os.path.join(outputFolder, outfn)
    plt.savefig(outfn)

from multiprocessing import Pool
p = Pool(3)
trigTags = ['L2dsaMu', 'L2rsaMu', 'L2MuCosmicSeed']
p.map(makingPlot, trigTags)
