#1/usr/bin/env python

import ROOT
import matplotlib.pyplot as plt
from root_pandas import read_root

def makingPlots(trigTag):
    inputDataName = 'MET_match.root'
    infn = '../test/{0}'.format(inputDataName)

   
    pf = read_root(infn, '{0}/mutree'.format(trigTag))
    pf['l1vl2']   = (pf.pT_L2 - pf.pT_L1) / pf.pT_L2
    pf['l1vReco'] = (pf.pT_reco - pf.pT_L1) / pf.pT_reco
    pf['l2vReco'] = (pf.pT_reco - pf.pT_L2) / pf.pT_reco

    ## select mu+
    df = pf.query('charge==-1')
    #################
    
    plt.style.use('ggplot')
    fig = plt.figure(figsize=(30,8))
    
    cutsLabel = [
            ('abs(dxy)<1',                    '|dxy|: [0, 1) cm'),
            ('abs(dxy)<10  and abs(dxy)>=1',  '|dxy|: [1, 10) cm'),
            ('abs(dxy)<30  and abs(dxy)>=10', '|dxy|: [10, 30) cm'),
            ('abs(dxy)<100 and abs(dxy)>=30', '|dxy|: [30, 100) cm'),
            ('abs(dxy)>100',                  '|dxy|: [100, inf) cm')
            ]

    ax1 = fig.add_subplot(1,3,1)
    for c, l in cutsLabel:
        ax1.hist(df.query(c)['l1vl2'], bins=50, range=(-1.05, 1.05), histtype='step', label=l)
       
    ax1.set_title('pT resolution of matched L2 trigger objects with matched L1 trigger objects')
    ax1.set_xlabel('(pT_L2 - pT_L1)/pT_L2')
    ax1.set_ylabel('Num. of Muons')
    
    ax2 = fig.add_subplot(1,3,2)
    for c, l in cutsLabel:
        ax2.hist(df.query(c)['l1vReco'], bins=50, range=(-1.05, 1.05), histtype='step', label=l)
    
    ax2.set_title('pT resolution of reco muons with matched L1 trigger objects')
    ax2.set_xlabel('(pT_reco - pT_L1)/pT_reco')
    ax2.set_ylabel('Num. of Muons')

    ax3 = fig.add_subplot(1,3,3)
    
    for c, l in cutsLabel:
        ax3.hist(df.query(c)['l2vReco'], bins=50, range=(-1.05,1.05), histtype='step', label=l)

    ax3.set_title('pT resolution of reco muons with matched L2 trigger objects')
    ax3.set_xlabel('(pT_reco - pT_L2)/pT_reco')
    ax3.set_ylabel('Num. of Muons')

    axes = [ax1, ax2, ax3]
    for ax in axes:
        ax.set_yscale('log')
        ax.legend(loc='best',fancybox=True, framealpha=0.5)
        ax.text(-1.4,5000, 'MET Run2018A PromptReco\n7.93/fb (Run314472-316271)', fontsize=10)

    outputFolder = '/eos/user/w/wsi/www/plots/L2Mu/{0}'.format(inputDataName.rstrip('.root'))
    import os
    if not os.path.isdir(outputFolder): os.mkdir(outputFolder)
    outfn = 'pTResolutionWithTriggerObject_{0}_muMinus.pdf'.format(trigTag)
    outfn = os.path.join(outputFolder, outfn)
    plt.savefig(outfn)

from multiprocessing import Pool
p = Pool(2)
#trigTags = ['doubleL2dsaMu', 'doubleL2rsaMu', 'L2dsaMu', 'L2rsaMu',
#        'doubleL2MuCosmicSeed', 'L2MuCosmicSeed']#, 'DSTdsaMu', 'DSTrsaMu']
trigTags = ['L2dsaMu' ,'L2MuCosmicSeed']
p.map(makingPlots, trigTags)
