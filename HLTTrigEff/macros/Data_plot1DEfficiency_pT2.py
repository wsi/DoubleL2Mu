#!/usr/bin/env python

# make 1D plot of leading muon of single muon path
# efficiency as a function of pT in bins

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.gStyle.SetLegendFont(42)
ROOT.gStyle.SetLabelSize(0.02, "xyz")

#inputDataName = 'Result_MET_trigAll_inZ.root'
#inputDataName = 'Result_MET_trigAll.root'
inputDataName = 'Result_SingleMuon.root'
infn = '../test/{0}'.format(inputDataName)
inf = ROOT.TFile(infn)

c = ROOT.TCanvas('c','',500,500)
c.SetGrid()

#effi = inf.Get('dL2Mu23dSA')
effi = inf.Get('doubleL2dsaMu')
#effi.SetMaximum(1.05)
#effi.SetMinimum(0)
effi.SetMarkerStyle(ROOT.kFullCircle)
effi.SetMarkerColorAlpha(ROOT.kRed, 1.)
effi.SetLineColorAlpha(ROOT.kRed, 1.)
effi.SetTitle(';subleading #mu p_{T} [GeV];Efficiency')
effi.Draw('ap')
ROOT.gPad.Update()
effi.GetPaintedGraph().SetMaximum(1.05)
effi.GetPaintedGraph().SetMinimum(0)

leg = ROOT.TLegend(0.4, 0.86, 0.88, 0.89)
leg.AddEntry(effi, 'HLT_DoubleL2Mu23NoVtx_2Cha_v', 'lep')
leg.Draw()

Label = ROOT.TPaveText(0.08, 0.9, 0.55, 0.95, 'NDC')
Label.AddText('SingleMuon 2018A PromptReco')
#Label.AddText('MET 2018A PromptReco')
Label.SetFillColor(0)
Label.SetLineColor(0)
Label.SetFillStyle(0)
Label.SetTextFont(62)
Label.SetTextAlign(13)
Label.SetTextSize(0.025)
Label.SetBorderSize(0)
Label.Draw()

Label1 = ROOT.TPaveText(0.58, 0.9, 0.92, 0.95, 'NDC')
Label1.AddText('4.051fb^{-1} (Run314472-315721)')
#Label1.AddText('7.93fb^{-1} (Run314472-316271)')
Label1.SetFillColor(0)
Label1.SetLineColor(0)
Label1.SetFillStyle(0)
Label1.SetTextFont(42)
Label1.SetTextAlign(33)
Label1.SetTextSize(0.023)
Label1.SetBorderSize(0)
Label1.Draw()

import os
outputFolder = '/eos/user/w/wsi/www/plots/L2Mu/MET'
outfn = 'doubleMuTriggerSingleMuoninROOT_bothMatch.pdf'
outfn = os.path.join(outputFolder, outfn)
c.Print(outfn)
