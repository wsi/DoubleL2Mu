#!/usr/bin/env python

import ROOT
ROOT.gROOT.SetBatch(True)
import numpy as np

infn = '../test/Result_MET_trigAll.root'
inf = ROOT.TFile(infn)

from plottingUtils import *
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
plt.style.use('ggplot')

fig = plt.figure(0)

source = inf.Get('dL2Mu23CSdSA')
dataPoints = extract1DTEff(source)
plt.errorbar(x=dataPoints['x'], y=dataPoints['y'],
        xerr=dataPoints['xerr'], yerr=dataPoints['yerr'],
        fmt='o', label='HLT_DoubleL2Mu23NoVtx_2Cha_CosmicSeed_v')

source = inf.Get('dL2Mu25CSdSA')
dataPoints = extract1DTEff(source)
plt.errorbar(x=dataPoints['x'], y=dataPoints['y'],
        xerr=dataPoints['xerr'], yerr=dataPoints['yerr'],
        fmt='o', label='HLT_DoubleL2Mu25NoVtx_2Cha_CosmicSeed_v')

source = inf.Get('dL2Mu25Eta2p4CSdSA')
dataPoints = extract1DTEff(source)
plt.errorbar(x=dataPoints['x'], y=dataPoints['y'],
        xerr=dataPoints['xerr'], yerr=dataPoints['yerr'],
        fmt='o', label='HLT_DoubleL2Mu25NoVtx_2Cha_CosmicSeed_Eta2p4_v')

source = inf.Get('dL2Mu30Eta2p4CSdSA')
dataPoints = extract1DTEff(source)
plt.errorbar(x=dataPoints['x'], y=dataPoints['y'],
        xerr=dataPoints['xerr'], yerr=dataPoints['yerr'],
        fmt='o', label='HLT_DoubleL2Mu30NoVtx_2Cha_CosmicSeed_Eta2p4_v')

plt.legend(loc='best', prop={'size': 8})
plt.xlabel('subleading dSA muon pT [GeV]')
plt.ylabel('Efficiency')
axes = plt.gca()
axes.set_ylim([0,1])
axes.xaxis.set_major_locator(ticker.MultipleLocator(10))
axes.yaxis.set_major_locator(ticker.MultipleLocator(0.1))

axes.text(0, 1, 'MET 2018A PromptReco', fontsize=10, fontweight='medium', family='sans-serif')
axes.text(100, 1, '7.93 /fb (Run314472-316271)', fontsize=8, family='sans-serif',
        horizontalalignment='right', verticalalignment='bottom')

outputFolder = '/eos/user/w/wsi/www/plots/L2Mu/OnlineTrigger'
import os
if not os.path.isdir(outputFolder): os.mkdir(outputFolder)
outfn = 'HLT_DoubleL2Mu_CosmicSeed.pdf'
outfn = os.path.join(outputFolder, outfn)
plt.savefig(outfn)
