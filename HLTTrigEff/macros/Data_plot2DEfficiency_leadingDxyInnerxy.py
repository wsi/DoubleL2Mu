#!/usr/bin/env python

# making plot of 2D trigger efficiency
# as a function of
# X: leading muon dxy
# Y: leading muon InnerPosition() xy

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.gStyle.SetLegendFont(42)
ROOT.gStyle.SetLabelSize(0.02, "xyz")
ROOT.gStyle.SetPaintTextFormat( "6.5f" )
ROOT.gStyle.SetPalette(112)



def makingPlot(trigTag):
    inputDataName = 'MET_trigAll.root'
    infn = '../test/{0}'.format(inputDataName)
    inf = ROOT.TFile(infn)
    c = ROOT.TCanvas('c{0}'.format(trigTag),'',500,500)

    from array import array
    xbins = array('d', [0, 1, 5,  10, 20, 30])
    ybins = array('d', [0, 1, 10, 30, 100, 400, 500, 650])
    origEff = ROOT.TEfficiency('origeff','', len(xbins)-1, xbins, len(ybins)-1, ybins)

    tr = inf.Get(trigTag + '/leadingMu')
    for mu in tr:
        if mu.pt<35: continue
        ## match with reco Mu
        if not mu.recoMuMatch: continue
        origEff.Fill(mu.passTrigger and mu.triggerMatch, abs(mu.dxy), mu.innerxy)

    kx = origEff.GetTotalHistogram().GetNbinsX()
    ky = origEff.GetTotalHistogram().GetNbinsY()
    
    data = {}
    for y in range(ky, 0, -1):
        for x in range(1, kx+1):
            gb = origEff.GetGlobalBin(x,y)
            data[(x,y)] = (origEff.GetEfficiency(gb),
                    (origEff.GetEfficiencyErrorUp(gb)+origEff.GetEfficiencyErrorLow(gb))/2)

    xfakeBins = array('d', list(range(len(xbins))))
    yfakeBins = array('d', list(range(len(ybins))))
    h = ROOT.TH2D('h',';Leading #mu |dxy| [cm];Leading #mu InnerPosition()xy [cm];Efficiency',
            len(xfakeBins)-1, xfakeBins, len(yfakeBins)-1, yfakeBins)

    for k in data:
        x,y = k
        val, err = data[k]
        h.SetBinContent(x, y, val)
        h.SetBinError(x, y, err)
    h.Draw('colz text e')
    h.SetMarkerSize(1.5)
    h.SetMarkerColor(0)
    h.GetXaxis().SetNdivisions(8)
    h.GetYaxis().SetNdivisions(8)
    h.GetZaxis().SetRangeUser(0, 1)
    for i, l in enumerate(xbins):
        h.GetXaxis().ChangeLabel(i+1, -1,-1,-1,-1,-1, str(l))
    for i, l in enumerate(ybins):
        h.GetYaxis().ChangeLabel(i+1, -1,-1,-1,-1,-1, str(l))

    ROOT.gPad.Update()
    h.GetListOfFunctions().FindObject('palette').SetX2NDC(0.92)

    Label0 = ROOT.TPaveText(0.08, 0.9, 0.55, 0.95, 'NDC')
    Label0.AddText('MET 2018A PromptReco')
    Label0.SetFillColor(0)
    Label0.SetLineColor(0)
    Label0.SetFillStyle(0)
    Label0.SetTextFont(62)
    Label0.SetTextAlign(13)
    Label0.SetTextSize(0.025)
    Label0.SetBorderSize(0)
    Label0.Draw()

    Label1 = ROOT.TPaveText(0.58, 0.9, 0.92, 0.95, 'NDC')
    Label1.AddText('7.93fb^{-1} (Run314472-316271)')
    Label1.SetFillColor(0)
    Label1.SetLineColor(0)
    Label1.SetFillStyle(0)
    Label1.SetTextFont(42)
    Label1.SetTextAlign(33)
    Label1.SetTextSize(0.023)
    Label1.SetBorderSize(0)
    Label1.Draw()

    outputFolder = '/eos/user/w/wsi/www/plots/L2Mu/{0}'.format(inputDataName.rstrip('.root'))
    #outputFolder = 'plots/L2Mu/{0}'.format(inputDataName.rstrip('.root'))
    import os
    if not os.path.isdir(outputFolder): os.mkdir(outputFolder)
    outfn = 'leadingDxyInnerxy_{0}.pdf'.format(trigTag)
    outfn = os.path.join(outputFolder, outfn)
    c.Print(outfn)

from multiprocessing import Pool
p = Pool(4)
#trigTags = ['L2Mu23dSA', 'L2Mu23rSA', 'L2Mu23dGM', 'L2Mu23CSdSA']
trigTags = ['L2Mu23dGM']
p.map(makingPlot, trigTags)
