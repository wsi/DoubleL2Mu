#!/usr/bin/env python

# calculate efficiency as a function of pT
# for different trigger paths, and store them
# in a root file

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.gStyle.SetLegendFont(42)
ROOT.gStyle.SetLabelSize(0.02, "xyz")

inputDataName = 'MET_trigAll.root'
#inputDataName = 'SingleMuon.root'
infn = '../test/{0}'.format(inputDataName)
inf = ROOT.TFile(infn)

#dirs = [x.GetName() for x in inf.GetListOfKeys()]
#dirs = ['doubleL2dsaMu']
dirs = ['dL2Mu23dSA']

trees = {}
for d in dirs:
    keys = [k.GetName() for k in getattr(inf, d).GetListOfKeys()]
    if 'subleadingMu' in keys:
        trees[d] = (inf.Get('{0}/{1}'.format(d, 'leadingMu')),
                inf.Get('{0}/{1}'.format(d, 'subleadingMu')))
    else:
        #trees[d] = inf.Get('{0}/{1}'.format(d, 'leadingMu'))
        pass

#outputFileName = 'Result_MET_trigAll.root'
outputFileName = 'Result_'+inputDataName
outfn = '../test/{0}'.format(outputFileName)
outf = ROOT.TFile(outfn, 'RECREATE')

for d, t in trees.iteritems():
    effi = ROOT.TEfficiency(d, '', 30, 0, 100)
    leadMuTriggerMatch = [mu.triggerMatch for mu in t[0]]
    for i, mu in enumerate(t[1]):
        effi.Fill(mu.passTrigger and mu.triggerMatch and leadMuTriggerMatch[i], mu.pt)
    effi.SetTitle('%s;p_{T} GeV;Efficiency' % d)
    effi.Write()

outf.Close()
print '%s generated!' % outfn
print 'Done.'
