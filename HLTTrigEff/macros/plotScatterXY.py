#1/usr/bin/env python

import ROOT

def makingPlot(trigTag):
    inputDataName = 'MET.root'
    infn = '../test/{0}'.format(inputDataName)

    inf = ROOT.TFile(infn)
    tr = inf.Get('{0}/leadingMu'.format(trigTag))

    from root_numpy import tree2array
    ar = tree2array(tr)
    denomDef = ar['pt']>35
    numerDef = denomDef & (ar['passTrigger']==1) & (ar['triggerMatch']==1)

    import matplotlib as mpl
    import matplotlib.pyplot as plt
    mpl.rcParams['agg.path.chunksize'] = 1000000
    plt.style.use('ggplot')
    fig = plt.figure()
    #ax = fig.add_subplot(1,1,1)

    var_x = 'dxy'
    #var_y = 'firstHitDis_xy'
    #var_y = 'vxy'
    var_y = 'innerxy'
    plt.plot(abs(ar[denomDef][var_x]), ar[denomDef][var_y], color='b', alpha=0.3,
            marker='.', markersize=2, linestyle='None', label='All')
    plt.plot(abs(ar[numerDef][var_x]), ar[numerDef][var_y], color='r', alpha=0.3,
            marker='.', markersize=2, linestyle='None', label='Triggered')

    plt.legend(loc='best')
    plt.xlabel('leading mu {0} [cm]'.format(var_x))
    plt.ylabel('leading mu {0} [cm]'.format(var_y))
    plt.xlim((0, 700))
    plt.ylim((0, 700))

    outputFolder = '/eos/user/w/wsi/www/plots/L2Mu/{0}'.format(inputDataName.rstrip('.root'))
    #outputFolder = 'plots/L2Mu/{0}'.format(inputDataName.rstrip('.root'))
    import os
    if not os.path.isdir(outputFolder): os.mkdir(outputFolder)
    outfn = 'scatter_{1}{2}_{0}.pdf'.format(trigTag, var_y, var_x)
    outfn = os.path.join(outputFolder, outfn)
    plt.savefig(outfn)#, dpi=100)

from multiprocessing import Pool
#p = Pool(3)
trigTags = ['L2dsaMu', 'L2rsaMu', 'L2MuCosmicSeed']
#p.map(makingPlot, trigTags)
for tt in trigTags:
    makingPlot(tt)
