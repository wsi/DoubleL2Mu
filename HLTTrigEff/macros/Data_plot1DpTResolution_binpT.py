#1/usr/bin/env python

import ROOT
import matplotlib.pyplot as plt
from root_pandas import read_root

def makingPlots(args):
    '''
    args -> (filename, trigTag, cutStr, outfn)
    '''
    inputDataName, trigTag, preCuts, outfn = args
    #inputDataName = 'triggermatch_MC130mm.root'
    #inputDataName = 'MET_match.root'
    infn = '../test/{0}'.format(inputDataName)

   
    pf = read_root(infn, '{0}/mutree'.format(trigTag))
    pf['l1vl2']   = (pf.pT_L2 - pf.pT_L1) / pf.pT_L2
    pf['l1vReco'] = (pf.pT_reco - pf.pT_L1) / pf.pT_reco
    pf['l2vReco'] = (pf.pT_reco - pf.pT_L2) / pf.pT_reco

    ## select on |eta| 
    #etaCut = 'abs(eta)<0.9'
    #etaCut = 'abs(eta)>=0.9 and abs(eta)<1.2'
    #dxyCut = 'abs(dxy)>1'
    #preCuts = ' and '.join([etaCut, dxyCut])
    df = pf.query(preCuts)
    #################
    
    plt.style.use('ggplot')
    fig = plt.figure(figsize=(30,8))
    
    cutsLabel = [
            ('pT_reco>=0 and pT_reco<20', 'reco pT [0, 20) GeV'),
            ('pT_reco>=20 and pT_reco<40', 'reco pT [20, 40) GeV'),
            ('pT_reco>=40 and pT_reco<60', 'reco pT [40, 60) GeV'),
            ('pT_reco>=60 and pT_reco<80', 'reco pT [60, 80) GeV'),
            ('pT_reco>=80 and pT_reco<100', 'reco pT [80, 100) GeV')
            ]

    ax1 = fig.add_subplot(1,3,1)
    for c, l in cutsLabel:
        ax1.hist(df.query(c)['l1vl2'], bins=50, range=(-1.05, 1.05), histtype='step', label=l)
       
    ax1.set_title('pT resolution of matched L2 trigger objects with matched L1 trigger objects')
    ax1.set_xlabel('(pT_L2 - pT_L1)/pT_L2')
    ax1.set_ylabel('Num. of Muons')
    
    ax2 = fig.add_subplot(1,3,2)
    for c, l in cutsLabel:
        ax2.hist(df.query(c)['l1vReco'], bins=50, range=(-1.05, 1.05), histtype='step', label=l)
    
    ax2.set_title('pT resolution of reco muons with matched L1 trigger objects')
    ax2.set_xlabel('(pT_reco - pT_L1)/pT_reco')
    ax2.set_ylabel('Num. of Muons')

    ax3 = fig.add_subplot(1,3,3)
    
    for c, l in cutsLabel:
        ax3.hist(df.query(c)['l2vReco'], bins=50, range=(-1.05,1.05), histtype='step', label=l)

    ax3.set_title('pT resolution of reco muons with matched L2 trigger objects')
    ax3.set_xlabel('(pT_reco - pT_L2)/pT_reco')
    ax3.set_ylabel('Num. of Muons')

    axes = [ax1, ax2, ax3]
    for ax in axes:
        ax.set_yscale('log')
        ax.legend(loc='best',fancybox=True, framealpha=0.5)
        #ax.text(-1.4,5000, 'MET Run2018A PromptReco\n7.93/fb (Run314472-316271)', fontsize=10)
        #ax.text(-1.4,5000, 'MET Run2018A PromptReco\n7.93/fb (Run314472-316271)', fontsize=10)

    outputFolder = '/eos/user/w/wsi/www/plots/L2Mu/{0}'.format(inputDataName.rstrip('.root'))
    import os
    if not os.path.isdir(outputFolder): os.mkdir(outputFolder)
    #outfn = 'MCpTResolutionWithTriggerObject_{0}_etalt0p9.pdf'.format(trigTag)
    #outfn = 'MCpTResolutionWithTriggerObject_{0}_eta0p9-1p2.pdf'.format(trigTag)
    #outfn = 'pTResolutionWithTriggerObject_{0}_eta1p2-2p0.pdf'.format(trigTag)
    outfn = os.path.join(outputFolder, outfn)
    plt.savefig(outfn)

from multiprocessing import Pool
#trigTags = ['doubleL2dsaMu', 'doubleL2rsaMu', 'L2dsaMu', 'L2rsaMu',
#        'doubleL2MuCosmicSeed', 'L2MuCosmicSeed']#, 'DSTdsaMu', 'DSTrsaMu']
fileNames = ['triggermatch_MC130mm.root', 'triggermatch_MC1300mm.root']
trigTags = ['doubleL2dsaMC' ,'doubleL2dsaCosmicSeedMC']
etaCutLabel = [('abs(eta)<0.9', 'etaLt0p9'),
               ('abs(eta)>=0.9 and abs(eta)<1.2', 'eta0p9-1p2'),
               ('abs(eta)>=1.2 and abs(eta)<2.0', 'eta1p2-2p0')]
overallCuts = ['abs(dxy)>1']
myargs = []
for f in fileNames:
    for t in trigTags:
        for etaCut, label in etaCutLabel:
            cuts = etaCut
            outf = 'MCpTResolutionWithTriggerObject_{0}_{1}.pdf'.format(t, label)
            if len(overallCuts):
                cuts = ' and '.join([etaCut] + overallCuts)
            myargs.append((f, t, cuts, outf))

for g in myargs: print g

p = Pool(len(myargs))
p.map(makingPlots, myargs)
