#!/usr/bin/env python

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)

inputFile = '../test/MET.root'
#inputFile = '../test/MC_125_20_13mm.root'
outputFile = '../test/efficiency_analyzer_MET.pdf'
triggerPath = 'L2MuCosmicSeed'
requireMassCut = False 

def get_effi(inputFile, triggerPath, requireMassCut):
    denom = ROOT.TH1D('denom', 'Effciency denominator', 30, 0, 100)
    numer = ROOT.TH1D('numer', 'Effciency numerator'  , 30, 0, 100)
    
    inf = ROOT.TFile(inputFile)
    source = inf.Get(triggerPath+'/displacedStandAloneMuons')
    for mu in source:
        if requireMassCut:
            if mu.passMassCut: denom.Fill(mu.pt)
            if mu.passMassCut and mu.passTrigger: numer.Fill(mu.pt)
        else:
            denom.Fill(mu.pt)
            if mu.passTrigger: numer.Fill(mu.pt)
    
    effi = ROOT.TGraphAsymmErrors()
    effi.BayesDivide(numer, denom)
    effi.SetMaximum(1.05)
    effi.SetMinimum(0)
    effi.GetXaxis().SetTitle('Subleading #mu p_{T} [GeV]')
    effi.GetXaxis().SetNdivisions(16)
    #effi.GetXaxis().SetLabelSize(0.02)
    effi.GetYaxis().SetTitle('Efficiency')
    effi.GetYaxis().SetNdivisions(16)
    
    effi.SetMarkerColorAlpha(ROOT.kBlue, 0.8)
    effi.SetMarkerStyle(ROOT.kFullCircle)
    effi.SetLineColorAlpha(ROOT.kBlue, 0.8)
    return effi

c = ROOT.TCanvas()
c.SetGrid()
effi = get_effi(inputFile, triggerPath, requireMassCut)
effi.Draw('ap')
c.Print(outputFile)
