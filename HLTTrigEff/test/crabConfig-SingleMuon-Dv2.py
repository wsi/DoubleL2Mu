from CRABClient.UserUtilities import config, getUsernameFromSiteDB
config = config()

import time
config.General.requestName = 'L2Mu_{0}'.format(time.strftime('%b%d-%H%M'))
config.General.workArea = 'crabProjects'
config.General.transferOutputs = True
config.General.transferLogs = False

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = '../python/EfficiencyAnalyzer3_cfg.py'

config.Data.inputDataset = '/SingleMuon/Run2018D-PromptReco-v2/AOD'
config.Data.inputDBS = 'global'
config.Data.splitting = 'LumiBased'
config.Data.unitsPerJob = 100
config.Data.lumiMask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions18/13TeV/PromptReco/Cert_314472-320887_13TeV_PromptReco_Collisions18_JSON_MuonPhys.txt'
#config.Data.runRange = '319625-319756' # '193093-194075'
config.Data.outLFNDirBase = '/store/user/%s/trigger/L2Mu/SingleMuon' % (getUsernameFromSiteDB())
config.Data.publication = False
config.Data.outputDatasetTag = 'recoTrig_18Dv2'

config.Site.storageSite = 'T3_US_FNALLPC'
