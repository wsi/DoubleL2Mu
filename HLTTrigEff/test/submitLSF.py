#!/usr/bin/env python
import os

#pd = 'SingleMuon'
pd = 'MET'

#outDir = 'MET_trigAll'
outDir = 'MET'
try:
    os.mkdir(outDir)
except OSError:
    pass

#frag = 'EfficiencyOnlineTrigger_cfg.py'
#frag = 'EfficiencyAnalyzer_cfg.py'
frag = 'TriggerMatchAnalyzer_cfg.py'

if not os.path.isdir(pd):
    os.mkdir(pd)

#os.system('voms-proxy-init -voms cms -valid 192:00')

for i, f in enumerate(os.listdir('../data/'+pd)):
    cmd = 'cmsRun ../python/{3} inputFiles_load=../data/{0}/{1} outputFile={4}/{2} pd={0}'.format(pd, f, f.replace('list','root'), frag, outDir)
    bJob = '''cd /afs/cern.ch/user/w/wsi/Workspace/public/lpcdm/CMSSW_10_1_2_patch2/src/DoubleL2Mu/HLTTrigEff/test
export X509_USER_PROXY=/afs/cern.ch/user/w/wsi/x509up_u70835
eval `scram runtime -sh` {0}'''.format(cmd)
    bCmd = 'bsub -q 8nh -R "rusage[mem=8000:pool=5000]" -J "{0}_{1}" "{2}"'.format(pd, i, bJob.replace('\n', ';'))
    #print bCmd
    #print
    #if i>1: break
    os.system(bCmd)
