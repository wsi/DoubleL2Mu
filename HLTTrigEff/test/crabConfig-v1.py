from CRABClient.UserUtilities import config, getUsernameFromSiteDB
config = config()

import time
config.General.requestName = 'L2Mu_{0}'.format(time.strftime('%b%d-%H%M'))
config.General.workArea = 'crab_projects'
config.General.transferOutputs = True
config.General.transferLogs = False

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = '../python/EfficiencyAnalyzer_cfg.py'

config.Data.inputDataset = '/MET/Run2018A-PromptReco-v1/AOD'
config.Data.inputDBS = 'global'
config.Data.splitting = 'LumiBased'
config.Data.unitsPerJob = 20
config.Data.lumiMask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions18/13TeV/PromptReco/Cert_314472-316271_13TeV_PromptReco_Collisions18_JSON.txt'
config.Data.runRange = '315257-316271' # '193093-194075'
config.Data.outLFNDirBase = '/store/user/%s/trigger/L2Mu/MET' % (getUsernameFromSiteDB())
config.Data.publication = False
config.Data.outputDatasetTag = 'DoubleL2MuHLTEffciencyNtuple'

config.Site.storageSite = 'T3_US_FNALLPC'
