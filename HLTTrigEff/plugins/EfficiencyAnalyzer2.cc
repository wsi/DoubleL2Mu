#include <memory>

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "DataFormats/Common/interface/TriggerResults.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerTypeDefs.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/TrackReco/interface/TrackFwd.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
#include "DataFormats/Math/interface/deltaR.h"

#include "TTree.h"
#include "TLorentzVector.h"

#define M_MU 0.10565837
#define M_Z 91.1876

class EfficiencyAnalyzer2 : public edm::one::EDAnalyzer<edm::one::WatchRuns, edm::one::SharedResources>
{
public:
  explicit EfficiencyAnalyzer2(const edm::ParameterSet&);
  ~EfficiencyAnalyzer2();

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
  virtual void beginJob() override;
  virtual void beginRun(edm::Run const &, edm::EventSetup const&) override;
  virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
  virtual void endRun(edm::Run const &, edm::EventSetup const&) override;
  virtual void endJob() override;

  const edm::EDGetTokenT<edm::TriggerResults> trigResults;
  const edm::EDGetTokenT<trigger::TriggerEvent> trigEvent;
  const std::string trigPath;
  std::string trigPathv;

  const edm::EDGetTokenT<reco::TrackCollection> muons;
  const std::string processName_;
  const bool verbose_;

  edm::Service<TFileService> fs;
  HLTConfigProvider hltConfig_;

  TTree *muTree;
  
  Int_t runNumber_, lumiBlock_;
  Long64_t eventNumber_;

  bool triggerFired;
  double tagMuPt, tagMuEta, tagMuPhi, tagMuDxy, tagMuVxy;
  double probeMuPt, probeMuEta, probeMuPhi, probeMuDxy, probeMuVxy;
  bool probeMuMatch, fromZ;
  float tnpdr, tnpInvM;
};

/// ctor and dtor
EfficiencyAnalyzer2::EfficiencyAnalyzer2(const edm::ParameterSet& iC)
:
trigResults(consumes<edm::TriggerResults>(iC.getParameter<edm::InputTag>("_trigResults"))),
trigEvent(consumes<trigger::TriggerEvent>(iC.getParameter<edm::InputTag>("_trigEvent"))),
trigPath(iC.getUntrackedParameter<std::string>("_trigPath")),
muons(consumes<reco::TrackCollection>(iC.getParameter<edm::InputTag>("_muons"))),
processName_(iC.getUntrackedParameter<std::string>("processName", "HLT")),
verbose_(iC.getUntrackedParameter<bool>("_verbose", false))
{
  usesResource("TFileService");
}


EfficiencyAnalyzer2::~EfficiencyAnalyzer2() {}

void
EfficiencyAnalyzer2::beginJob() {
  // making tree...
  muTree = fs->make<TTree>("mu","");

  muTree->Branch("run",   &runNumber_,  "run/I");
  muTree->Branch("lumi",  &lumiBlock_,  "lumi/I");
  muTree->Branch("event", &eventNumber_,"event/l");

  muTree->Branch("triggerFired", &triggerFired, "triggerFired/O");
  muTree->Branch("tagMuPt",  &tagMuPt,  "tagMuPt/D");
  muTree->Branch("tagMuEta", &tagMuEta, "tagMuEta/D");
  muTree->Branch("tagMuPhi", &tagMuPhi, "tagMuPhi/D");
  muTree->Branch("tagMuDxy", &tagMuDxy, "tagMuDxy/D");
  muTree->Branch("tagMuVxy", &tagMuVxy, "tagMuVxy/D");

  muTree->Branch("probeMuPt",  &probeMuPt,  "probeMuPt/D");
  muTree->Branch("probeMuEta", &probeMuEta, "probeMuEta/D");
  muTree->Branch("probeMuPhi", &probeMuPhi, "probeMuPhi/D");
  muTree->Branch("probeMuDxy", &probeMuDxy, "probeMuDxy/D");
  muTree->Branch("probeMuVxy", &probeMuVxy, "probeMuVxy/D");

  muTree->Branch("probeMuMatch", &probeMuMatch, "probeMuMatch/O");
  muTree->Branch("fromZ", &fromZ, "fromZ/O");
  muTree->Branch("tnpdr", &tnpdr, "tnpdr/F");
  muTree->Branch("tnpInvM", &tnpInvM, "tnpInvM/F");
}

void
EfficiencyAnalyzer2::beginRun(edm::Run const &iRun, edm::EventSetup const& iSetup)
{
  using namespace std;
  using namespace edm;

  bool changed(true);
  if (hltConfig_.init(iRun,iSetup,processName_,changed)) {
    if (changed) {
      LogInfo("EfficiencyAnalyzer2")<<"EfficiencyAnalyzer2::beginRun: "<<"hltConfig init for Run"<<iRun.run();
      hltConfig_.dump("ProcessName");
      hltConfig_.dump("GlobalTag");
      hltConfig_.dump("TableName");
    }
  } else {
    LogError("EfficiencyAnalyzer2")<<"EfficiencyAnalyzer2::analyze: config extraction failure with processName ";
  }

}

void
EfficiencyAnalyzer2::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
  using namespace std;
  using namespace edm;

  /* reset */
  triggerFired = false;
  probeMuMatch = false;
  fromZ = false;
  tnpdr = 9999.;

  /* event info */
  runNumber_   = iEvent.id().run();
  lumiBlock_   = iEvent.isRealData() ? iEvent.luminosityBlock() : -1;
  eventNumber_ = iEvent.id().event();

  Handle<reco::TrackCollection> muonsH;
  iEvent.getByToken(muons, muonsH);
  if (!muonsH.isValid()) { LogError("EfficiencyAnalyzer2")<<"reco::TrackCollection invalid!"<<endl; return; }

  vector<reco::TrackRef> muTkRef{};
  for (size_t iM(0); iM!=muonsH->size(); ++iM) {
    muTkRef.emplace_back(muonsH, iM);
  }

  /* general selection */
  auto generalSelection = [](const auto& t){
    bool pass = t->pt() > 5
            && abs(t->eta()) < 2
            && t->hitPattern().numberOfValidMuonHits() > 16
            && t->hitPattern().muonStationsWithValidHits() > 1
            && t->normalizedChi2() < 10;
    return !pass;
  };
  muTkRef.erase(std::remove_if(muTkRef.begin(), muTkRef.end(), generalSelection), muTkRef.end());
  if (muTkRef.size() < 2) return;

  if (verbose_) { cout<<endl; }
  if (verbose_) { cout<<"Event "<<eventNumber_<<" == "; }
  if (verbose_) { cout<<"Num mu pass general selection: "<<muTkRef.size()<<endl; }

  auto sort_by_pt_gt = [](const auto& lhs, const auto&rhs){ return lhs->pt() > rhs->pt(); };
  std::sort(muTkRef.begin(), muTkRef.end(), sort_by_pt_gt);

  /* selecting trigger objects */
  Handle<edm::TriggerResults> trigResultsH;
  iEvent.getByToken(trigResults, trigResultsH);
  if (!trigResultsH.isValid()) { LogError("EfficiencyAnalyzer2")<<"edm::TriggerResult invalid!"<<endl; return; }
  Handle<trigger::TriggerEvent> trigEventH;
  iEvent.getByToken(trigEvent, trigEventH);
  if (!trigEventH.isValid()) { LogError("EfficiencyAnalyzer2")<<"trigger::TriggerEvent invalid!"<<endl; return; }

  const vector<string>& pathNames = hltConfig_.triggerNames();
  unsigned int sizePaths = pathNames.size();

  /// checking if trigger fired 
  const vector<string> matchedPaths(hltConfig_.restoreVersion(pathNames, trigPath));
  if (matchedPaths.size() == 0) {
    LogError("EfficiencyAnalyzer2")<<"Could not find matched full trigger path with "<<trigPath<<"!"<<endl;
    return;
  }
  trigPathv = matchedPaths[0];
  if (hltConfig_.triggerIndex(trigPathv) >= hltConfig_.size()) {
    LogError("EfficiencyAnalyzer2")<<trigPathv<<"not found!"<<endl;
    return;
  }
  triggerFired = trigResultsH->accept(hltConfig_.triggerIndex(trigPathv));

  if (verbose_) { cout<<"  Trigger: "<<trigPathv<<" "<<int(triggerFired)<<endl; }

  /// First, picking out last filters of all accepted trig paths
  vector<trigger::size_type> lastFilterIndices{};
  for (unsigned int pathIdx(0); pathIdx!=sizePaths; ++pathIdx) {
    if (!trigResultsH->wasrun(pathIdx) or trigResultsH->error(pathIdx)) { continue; }
    if (!trigResultsH->accept(pathIdx)) { continue; }
    
    const vector<string>& nameModules = hltConfig_.moduleLabels(pathIdx);
    unsigned int sizeModules = nameModules.size();
    unsigned int iM = sizeModules;
    while ( iM>0 ) {
      const string& nameFilter = nameModules[--iM];
      if (hltConfig_.moduleEDMType(nameFilter) != "EDFilter") { continue; }
      bool saveTags = hltConfig_.saveTags(nameFilter);
      if (!saveTags) { continue; }
      
      trigger::size_type lastFilterIndex = trigEventH->filterIndex(InputTag(nameFilter, "", processName_));
      lastFilterIndices.push_back(lastFilterIndex);
      break;
    }
  }

  if (verbose_) { cout<< "  Size of last filters of accepted trigger paths: "<<lastFilterIndices.size()<<endl; }

  /// Then, construct trigger object collection associated with last filters
  vector<trigger::size_type> muTrigObjsWithLastFilterIndices{};
  for (const trigger::size_type& iF : lastFilterIndices) {
    if (iF >= trigEventH->sizeFilters()) { continue; }

    const trigger::Keys& keys = trigEventH->filterKeys(iF);
    const trigger::Vids& types= trigEventH->filterIds(iF);

    for (size_t iK(0); iK!=keys.size(); ++iK) {
      if (types[iK]!=trigger::TriggerObjectType::TriggerL1Mu and
          types[iK]!=trigger::TriggerObjectType::TriggerMuon) { continue; }
      if (find(muTrigObjsWithLastFilterIndices.begin(), muTrigObjsWithLastFilterIndices.end(), keys[iK])
          != muTrigObjsWithLastFilterIndices.end()) { continue; }
      muTrigObjsWithLastFilterIndices.push_back(keys[iK]);
    }
  }

  if (verbose_) { cout<<"  Size of total mu trigger objects: "<<muTrigObjsWithLastFilterIndices.size()<<endl; }

  /* Loop reco muon tracks to find tag and probe muons */
  vector<unsigned int> tagMuIds{};
  for (const auto& mutag : muTkRef) {
    /// tag muons need to match with trigger object
    bool tagMatch(false);
    for (const trigger::size_type& toIdx : muTrigObjsWithLastFilterIndices) {
      float dr = deltaR( (trigEventH->getObjects())[toIdx], *mutag.get());
      if (dr >= 0.3) { continue; }
      tagMatch = true;
      break;
    }
    if (!tagMatch) { continue; }
    tagMuPt  = mutag->pt();
    tagMuEta = mutag->eta();
    tagMuPhi = mutag->phi();
    tagMuDxy = mutag->dxy();
    tagMuVxy = sqrt(mutag->vx()*mutag->vx() + mutag->vy()*mutag->vy());
    TLorentzVector muTagLv;
    muTagLv.SetPxPyPzE(mutag->px(), mutag->py(), mutag->pz(), sqrt(mutag->p()*mutag->p() + M_MU*M_MU));
    tagMuIds.push_back(mutag.key());

    if (verbose_) { cout<<">>Tag muon - pt: "<<tagMuPt<<" eta: "<<tagMuEta<<" phi: "<<tagMuPhi<<endl; }

    /// look for any probe muon
    for (const auto& muprobe : muTkRef) {
      if (muprobe == mutag) { continue; }
      if (find(tagMuIds.begin(), tagMuIds.end(), muprobe.key()) != tagMuIds.end()) { continue; }

      TLorentzVector muProbeLv;
      muProbeLv.SetPxPyPzE(muprobe->px(), muprobe->py(), muprobe->pz(), sqrt(muprobe->p()*muprobe->p() + M_MU*M_MU));

      /// mass constraint
      tnpInvM = (muTagLv+muProbeLv).M();
      fromZ = false;
      if (abs(M_Z - tnpInvM) <= 10) { fromZ = true; }

      probeMuPt  = muprobe->pt();
      probeMuEta = muprobe->eta();
      probeMuPhi = muprobe->phi();
      probeMuDxy = muprobe->dxy();
      probeMuVxy = sqrt(muprobe->vx()*muprobe->vx() + muprobe->vy()*muprobe->vy());

      /// check if probe match trigger object
      bool probeMatch(false);
      for (const trigger::size_type& toIdx : muTrigObjsWithLastFilterIndices) {
        float dr = deltaR( (trigEventH->getObjects())[toIdx], *muprobe.get());
        if (dr >= 0.3) { continue; }
        probeMatch = true;
        break;
      }

      probeMuMatch = probeMatch;

      tnpdr = deltaR(*mutag.get(), *muprobe.get());

      if (verbose_) {
        cout<<"-->>Probe muon - pt: "<<probeMuPt<<" eta: "<<probeMuEta<<" phi: "<<probeMuPhi<<" invM: "
          <<tnpInvM<<" trigmatch: "<<int(probeMatch)<<" deltaR: "<<tnpdr<<endl;
      }

      muTree->Fill();

    }
  }

  return;
}


void
EfficiencyAnalyzer2::endRun(edm::Run const &iRun, edm::EventSetup const& iSetup) {}

void
EfficiencyAnalyzer2::endJob() {}


#include "FWCore/ParameterSet/interface/ConfigurationDescriptions.h"
#include "FWCore/ParameterSet/interface/ParameterSetDescription.h"
void
EfficiencyAnalyzer2::fillDescriptions(edm::ConfigurationDescriptions& descriptions)
{
  edm::ParameterSetDescription desc;
  desc.addUntracked<std::string>("_trigPath", "HLT_DoubleL2Mu23NoVtx_2Cha");
  desc.add<edm::InputTag>("_trigEvent", edm::InputTag("hltTriggerSummaryAOD","","HLT"));
  desc.add<edm::InputTag>("_muons", edm::InputTag("displacedStandAloneMuons"));
  desc.addUntracked<std::string>("processName", "HLT");
  desc.addUntracked<bool>("_verbose", false);
  desc.add<edm::InputTag>("_trigResults", edm::InputTag("TriggerResults","","HLT"));
  descriptions.add("effiana2", desc);
}

#include "FWCore/Framework/interface/MakerMacros.h"
DEFINE_FWK_MODULE(EfficiencyAnalyzer2);
