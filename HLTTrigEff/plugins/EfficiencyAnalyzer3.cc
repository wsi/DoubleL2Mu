/**
 * EfficiencyAnalyzer3
 * ===================
 * Tuplizer for displacedStandAloneMuons,
 * - applying some quality cuts
 * - matching trigger objects exclusivly (dR<0.3)
 * - save:
 *   - event information (run, lumi, event)
 *   - trigger bits of interested path
 *   - dSA muon charge, pt, eta, phi, dxy
 *   - matched trigger object pt, eta phi
 */

#include <memory>

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "DataFormats/Common/interface/TriggerResults.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerTypeDefs.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/TrackReco/interface/TrackFwd.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
#include "DataFormats/Math/interface/deltaR.h"

#include "TTree.h"
//#include "TLorentzVector.h"
//
//#define M_MU 0.10565837
//#define M_Z 91.1876

class EfficiencyAnalyzer3 : public edm::one::EDAnalyzer<edm::one::WatchRuns, edm::one::SharedResources>
{
public:
  explicit EfficiencyAnalyzer3(const edm::ParameterSet&);
  ~EfficiencyAnalyzer3();

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
  virtual void beginJob() override;
  virtual void beginRun(edm::Run const &, edm::EventSetup const&) override;
  virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
  virtual void endRun(edm::Run const &, edm::EventSetup const&) override;
  virtual void endJob() override;

  const edm::EDGetTokenT<edm::TriggerResults> trigResults;
  const edm::EDGetTokenT<trigger::TriggerEvent> trigEvent;
  const std::string trigPath;
  std::string trigPathv;

  const edm::EDGetTokenT<reco::TrackCollection> muons;
  const std::string processName_;
  const bool verbose_;

  edm::Service<TFileService> fs;
  HLTConfigProvider hltConfig_;

  TTree *muTree;
  
  Int_t runNumber_, lumiBlock_;
  Long64_t eventNumber_;

  bool triggerFired;
  std::vector<int> muCharge;
  std::vector<float> muPt, muEta, muPhi, muDxy, muVxy; // reco dSA mu
  std::vector<float> toPt, toEta, toPhi;               // mu trigger objects
};

/// ctor and dtor
EfficiencyAnalyzer3::EfficiencyAnalyzer3(const edm::ParameterSet& iC)
:
trigResults(consumes<edm::TriggerResults>(iC.getParameter<edm::InputTag>("_trigResults"))),
trigEvent(consumes<trigger::TriggerEvent>(iC.getParameter<edm::InputTag>("_trigEvent"))),
trigPath(iC.getUntrackedParameter<std::string>("_trigPath")),
muons(consumes<reco::TrackCollection>(iC.getParameter<edm::InputTag>("_muons"))),
processName_(iC.getUntrackedParameter<std::string>("processName", "HLT")),
verbose_(iC.getUntrackedParameter<bool>("_verbose", false))
{
  usesResource("TFileService");
}


EfficiencyAnalyzer3::~EfficiencyAnalyzer3() {}

void
EfficiencyAnalyzer3::beginJob() {
  // making tree...
  muTree = fs->make<TTree>("mu","");

  muTree->Branch("run",   &runNumber_,  "run/I");
  muTree->Branch("lumi",  &lumiBlock_,  "lumi/I");
  muTree->Branch("event", &eventNumber_,"event/l");

  muTree->Branch("triggerFired", &triggerFired, "triggerFired/O");
  muTree->Branch("recoCharge",  &muCharge);
  muTree->Branch("recoPt",  &muPt);
  muTree->Branch("recoEta", &muEta);
  muTree->Branch("recoPhi", &muPhi);
  muTree->Branch("recoDxy", &muDxy);
  muTree->Branch("recoVxy", &muVxy);
  muTree->Branch("trigPt",  &toPt);
  muTree->Branch("trigEta", &toEta);
  muTree->Branch("trigPhi", &toPhi);

}

void
EfficiencyAnalyzer3::beginRun(edm::Run const &iRun, edm::EventSetup const& iSetup)
{
  using namespace std;
  using namespace edm;

  bool changed(true);
  if (hltConfig_.init(iRun,iSetup,processName_,changed)) {
    if (changed) {
      LogInfo("EfficiencyAnalyzer3")<<"EfficiencyAnalyzer3::beginRun: "<<"hltConfig init for Run"<<iRun.run();
      hltConfig_.dump("ProcessName");
      hltConfig_.dump("GlobalTag");
      hltConfig_.dump("TableName");
    }
  } else {
    LogError("EfficiencyAnalyzer3")<<"EfficiencyAnalyzer3::analyze: config extraction failure with processName ";
  }

}

void
EfficiencyAnalyzer3::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
  using namespace std;
  using namespace edm;

  /* reset */
  triggerFired = false;

  /* event info */
  runNumber_   = iEvent.id().run();
  lumiBlock_   = iEvent.isRealData() ? iEvent.luminosityBlock() : -1;
  eventNumber_ = iEvent.id().event();

  Handle<reco::TrackCollection> muonsH;
  iEvent.getByToken(muons, muonsH);
  if (!muonsH.isValid()) { LogError("EfficiencyAnalyzer3")<<"reco::TrackCollection invalid!"<<endl; return; }

  vector<reco::TrackRef> muTkRef{};
  for (size_t iM(0); iM!=muonsH->size(); ++iM) {
    muTkRef.emplace_back(muonsH, iM);
  }

  /* general selection */
  auto generalSelection = [](const auto& t){
    bool pass = t->pt() > 5
            && abs(t->eta()) < 2
            && t->hitPattern().numberOfValidMuonHits() > 16
            && t->hitPattern().muonStationsWithValidHits() > 1
            && t->normalizedChi2() < 10;
    return !pass;
  };
  muTkRef.erase(std::remove_if(muTkRef.begin(), muTkRef.end(), generalSelection), muTkRef.end());
  if (muTkRef.size() < 2) return;

  if (verbose_) { cout<<endl; }
  if (verbose_) { cout<<"Event "<<eventNumber_<<" == "; }
  if (verbose_) { cout<<"Num mu pass general selection: "<<muTkRef.size()<<endl; }

  auto sort_by_pt_gt = [](const auto& lhs, const auto&rhs){ return lhs->pt() > rhs->pt(); };
  std::sort(muTkRef.begin(), muTkRef.end(), sort_by_pt_gt);

  /* clear container */
  muCharge.clear();
  muPt.clear(); muEta.clear(); muPhi.clear();
  muDxy.clear(); muVxy.clear();
  toPt.clear(); toEta.clear(); toPhi.clear();


  /* selecting trigger objects */
  Handle<edm::TriggerResults> trigResultsH;
  iEvent.getByToken(trigResults, trigResultsH);
  if (!trigResultsH.isValid()) { LogError("EfficiencyAnalyzer3")<<"edm::TriggerResult invalid!"<<endl; return; }
  Handle<trigger::TriggerEvent> trigEventH;
  iEvent.getByToken(trigEvent, trigEventH);
  if (!trigEventH.isValid()) { LogError("EfficiencyAnalyzer3")<<"trigger::TriggerEvent invalid!"<<endl; return; }

  const vector<string>& pathNames = hltConfig_.triggerNames();
  unsigned int sizePaths = pathNames.size();

  /// checking if trigger fired 
  const vector<string> matchedPaths(hltConfig_.restoreVersion(pathNames, trigPath));
  if (matchedPaths.size() == 0) {
    LogError("EfficiencyAnalyzer3")<<"Could not find matched full trigger path with "<<trigPath<<"!"<<endl;
    return;
  }
  trigPathv = matchedPaths[0];
  if (hltConfig_.triggerIndex(trigPathv) >= hltConfig_.size()) {
    LogError("EfficiencyAnalyzer3")<<trigPathv<<"not found!"<<endl;
    return;
  }
  triggerFired = trigResultsH->accept(hltConfig_.triggerIndex(trigPathv));

  if (verbose_) { cout<<"  Trigger: "<<trigPathv<<" "<<int(triggerFired)<<endl; }

  /// First, picking out last filters of all accepted trig paths
  vector<trigger::size_type> lastFilterIndices{};
  for (unsigned int pathIdx(0); pathIdx!=sizePaths; ++pathIdx) {
    if (!trigResultsH->wasrun(pathIdx) or trigResultsH->error(pathIdx)) { continue; }
    if (!trigResultsH->accept(pathIdx)) { continue; }
    
    const vector<string>& nameModules = hltConfig_.moduleLabels(pathIdx);
    unsigned int sizeModules = nameModules.size();
    unsigned int iM = sizeModules;
    while ( iM>0 ) {
      const string& nameFilter = nameModules[--iM];
      if (hltConfig_.moduleEDMType(nameFilter) != "EDFilter") { continue; }
      bool saveTags = hltConfig_.saveTags(nameFilter);
      if (!saveTags) { continue; }
      
      trigger::size_type lastFilterIndex = trigEventH->filterIndex(InputTag(nameFilter, "", processName_));
      lastFilterIndices.push_back(lastFilterIndex);
      break;
    }
  }

  if (verbose_) { cout<< "  Size of last filters of accepted trigger paths: "<<lastFilterIndices.size()<<endl; }

  /// Then, construct trigger object collection associated with last filters
  vector<trigger::size_type> muTrigObjsWithLastFilterIndices{};
  for (const trigger::size_type& iF : lastFilterIndices) {
    if (iF >= trigEventH->sizeFilters()) { continue; }

    const trigger::Keys& keys = trigEventH->filterKeys(iF);
    const trigger::Vids& types= trigEventH->filterIds(iF);

    for (size_t iK(0); iK!=keys.size(); ++iK) {
      if (types[iK]!=trigger::TriggerObjectType::TriggerL1Mu and
          types[iK]!=trigger::TriggerObjectType::TriggerMuon) { continue; }
      if (find(muTrigObjsWithLastFilterIndices.begin(), muTrigObjsWithLastFilterIndices.end(), keys[iK])
          != muTrigObjsWithLastFilterIndices.end()) { continue; }
      muTrigObjsWithLastFilterIndices.push_back(keys[iK]);
    }
  }

  if (verbose_) { cout<<"  Size of total mu trigger objects: "<<muTrigObjsWithLastFilterIndices.size()<<endl; }

  /* match offliine muon with trigger object */
  vector<trigger::size_type> matchedMuTrigObjsWithLastFilterIndices{};
  for (const auto& m : muTkRef) {

    bool findMatchedTrigObj = false;
    for (const auto toId : muTrigObjsWithLastFilterIndices) {
      if (find(matchedMuTrigObjsWithLastFilterIndices.begin(), 
               matchedMuTrigObjsWithLastFilterIndices.end(), toId)
            != matchedMuTrigObjsWithLastFilterIndices.end()) continue;
      float dR = deltaR(*(m.get()), (trigEventH->getObjects())[toId]);
      if (dR > 0.3) continue;
      matchedMuTrigObjsWithLastFilterIndices.push_back(toId);
      findMatchedTrigObj = true;
      break;
    }

    if (!findMatchedTrigObj) continue;

    // Filling reco muon
    muCharge.push_back(m->charge());
    muPt.push_back(m->pt());
    muEta.push_back(m->eta());
    muPhi.push_back(m->phi());
    muDxy.push_back(m->dxy());
    muVxy.push_back(sqrt(m->vx()*m->vx() + m->vy()*m->vy()));

    // Filling trigObj
    const auto& muTo = (trigEventH->getObjects())[matchedMuTrigObjsWithLastFilterIndices.back()];
    toPt.push_back(muTo.pt());
    toEta.push_back(muTo.eta());
    toPhi.push_back(muTo.phi());

  }

  muTree->Fill();

  return;
}


void
EfficiencyAnalyzer3::endRun(edm::Run const &iRun, edm::EventSetup const& iSetup) {}

void
EfficiencyAnalyzer3::endJob() {}


#include "FWCore/ParameterSet/interface/ConfigurationDescriptions.h"
#include "FWCore/ParameterSet/interface/ParameterSetDescription.h"
void
EfficiencyAnalyzer3::fillDescriptions(edm::ConfigurationDescriptions& descriptions)
{
  edm::ParameterSetDescription desc;
  desc.addUntracked<std::string>("_trigPath", "HLT_DoubleL2Mu23NoVtx_2Cha");
  desc.add<edm::InputTag>("_trigEvent", edm::InputTag("hltTriggerSummaryAOD","","HLT"));
  desc.add<edm::InputTag>("_muons", edm::InputTag("displacedStandAloneMuons"));
  desc.addUntracked<std::string>("processName", "HLT");
  desc.addUntracked<bool>("_verbose", false);
  desc.add<edm::InputTag>("_trigResults", edm::InputTag("TriggerResults","","HLT"));
  descriptions.add("effiana3", desc);
}

#include "FWCore/Framework/interface/MakerMacros.h"
DEFINE_FWK_MODULE(EfficiencyAnalyzer3);
