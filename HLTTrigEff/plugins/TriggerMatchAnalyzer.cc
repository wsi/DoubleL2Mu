#include <memory>

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "DataFormats/Common/interface/TriggerResults.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/TrackReco/interface/TrackFwd.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
#include "DataFormats/Math/interface/deltaR.h"

#include "TTree.h"
#include "TLorentzVector.h"


class TriggerMatchAnalyzer : public edm::one::EDAnalyzer<edm::one::WatchRuns, edm::one::SharedResources>
{
public:
  explicit TriggerMatchAnalyzer(const edm::ParameterSet&);
  ~TriggerMatchAnalyzer();

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

  
private:
  virtual void beginJob() override;
  virtual void beginRun(edm::Run const &, edm::EventSetup const&) override;
  virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
  virtual void endRun(edm::Run const &, edm::EventSetup const&) override;
  virtual void endJob() override;

  template<typename T>
  bool triggerMatch(const trigger::TriggerObject&, const T&) const;

  const edm::EDGetTokenT<edm::TriggerResults> trigResults;
  const std::string trigPath;
  std::string trigPathv;
  const edm::EDGetTokenT<trigger::TriggerEvent> trigEvent;
  // const std::vector<std::string> trigFilters;

  const edm::EDGetTokenT<reco::TrackCollection> muons;
  const edm::InputTag l1trigFilter;
  const edm::InputTag l2trigFilter;

  edm::Service<TFileService> fs;
  unsigned int nmuons_;
  int charge;
  double eta, phi, pt, dxy, vxy;
  double L1fpt, L1feta, L1fphi;
  double L2fpt, L2feta, L2fphi;

  std::string processName_;
  bool verbose_;
  HLTConfigProvider hltConfig_;
  TTree *muTree;

  Int_t nMuonInCollection_;
  Int_t runNumber_, lumiBlock_;
  Long64_t eventNumber_;
};

template<typename T>
bool
TriggerMatchAnalyzer::triggerMatch(const trigger::TriggerObject& to, const T& obj) const
{
  double deta = to.eta()-obj->eta();
  double dphi = to.phi()-obj->phi();
  double dR = std::sqrt(deta*deta + dphi*dphi);
  double dPtRel = abs(to.pt()-obj->pt())/obj->pt();

  return (dR<0.3 && dPtRel<1);
}

TriggerMatchAnalyzer::TriggerMatchAnalyzer(const edm::ParameterSet& iC)
:
trigResults(consumes<edm::TriggerResults>(iC.getParameter<edm::InputTag>("_trigResults"))),
trigPath(iC.getUntrackedParameter<std::string>("_trigPath")),
trigEvent(consumes<trigger::TriggerEvent>(iC.getParameter<edm::InputTag>("_trigEvent"))),
muons(consumes<reco::TrackCollection>(iC.getParameter<edm::InputTag>("_muons"))),
l1trigFilter(iC.getUntrackedParameter<edm::InputTag>("_L1TrigFilter")),
l2trigFilter(iC.getUntrackedParameter<edm::InputTag>("_L2TrigFilter")),
nmuons_(iC.getUntrackedParameter<unsigned int>("nmuons")),
processName_(iC.getUntrackedParameter<std::string>("processName", "HLT")),
verbose_(iC.getUntrackedParameter<bool>("verbose", false))
{
  usesResource("TFileService");
}


TriggerMatchAnalyzer::~TriggerMatchAnalyzer() {}


void
TriggerMatchAnalyzer::beginRun(edm::Run const &iRun, edm::EventSetup const& iSetup)
{
  using namespace std;
  using namespace edm;

  bool changed(true);
  if (hltConfig_.init(iRun,iSetup,processName_,changed)) {
    if (changed) {
      LogInfo("TriggerMatchAnalyzer")<<"TriggerMatchAnalyzer::beginRun: "<<"hltConfig init for Run"<<iRun.run();
      hltConfig_.dump("ProcessName");
      hltConfig_.dump("GlobalTag");
      hltConfig_.dump("TableName");
    }
  } else {
    LogError("TriggerMatchAnalyzer")<<"TriggerMatchAnalyzer::analyze: config extraction failure with processName ";
  }

}

void
TriggerMatchAnalyzer::endRun(edm::Run const &iRun, edm::EventSetup const& iSetup) {}


void
TriggerMatchAnalyzer::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
  using namespace edm;
  using namespace std;

  /* event info */
  runNumber_   = iEvent.id().run();
  lumiBlock_   = iEvent.isRealData() ? iEvent.luminosityBlock() : -1;
  eventNumber_ = iEvent.id().event();
  if (verbose_) {
    cout<<endl;
    cout<< "Event: "<<eventNumber_<<" ++ ";
  }

  Handle<reco::TrackCollection> muonsH;
  iEvent.getByToken(muons, muonsH);

  nMuonInCollection_ = muonsH->size();
  //cout<<"numMuons: "<<nMuonInCollection_<<" ++ ";

  std::vector<reco::TrackRef> MuTkRef{};
  for (size_t iM(0); iM!=muonsH->size(); ++iM) {
    MuTkRef.emplace_back(muonsH, iM);
  }

  /* general denominator selection condition */
  auto generalSelection = [](const auto& t){
    bool pass = t->pt() > 5
            && abs(t->eta()) < 2
            && t->hitPattern().numberOfValidMuonHits() > 16
            && t->hitPattern().muonStationsWithValidHits() > 1
            && t->normalizedChi2() < 10;
    return !pass;
  };
  MuTkRef.erase(std::remove_if(MuTkRef.begin(), MuTkRef.end(), generalSelection), MuTkRef.end());
  if (MuTkRef.size() < nmuons_) return;
  //cout<<"numMuon passing general selections: "<<MuTkRef.size()<<" ++ ";

  auto sort_by_pt_gt = [](const auto& lhs, const auto&rhs){return lhs->pt()>rhs->pt();};
  std::sort(MuTkRef.begin(), MuTkRef.end(), sort_by_pt_gt);

  /* trigger fire check */
  Handle<edm::TriggerResults> trigResultsH;
  iEvent.getByToken(trigResults, trigResultsH);
  Handle<trigger::TriggerEvent> trigEventH;
  iEvent.getByToken(trigEvent, trigEventH);

  const edm::TriggerNames& trigNames = iEvent.triggerNames(*trigResultsH);

  //std::cout<<"_____________________________\n";
  //for (const auto& tn : trigNames.triggerNames()) {
  //    std::cout<<tn<<std::endl;
  //}
  //std::cout<<"_____________________________\n";

  const vector<string> matchedPaths(hltConfig_.restoreVersion(trigNames.triggerNames(), trigPath));
  if (matchedPaths.size() == 0) { return; } // no matched trigger version found
  trigPathv = matchedPaths[0];
  //cout<<"Trigger Path found: "<<trigPathv<<" ++ ";

  // abort on invalid trigger name
  if (trigNames.triggerIndex(trigPathv) == trigNames.size()) return;

  bool triggerFired(trigResultsH->accept(trigNames.triggerIndex(trigPathv)));
  if (!triggerFired) return;
  //cout<<"Trigger FIRED! ++ ";

  // saved tags
  const vector<string>& savedTags(hltConfig_.saveTagsModules(trigPathv));
  if (verbose_) {
    cout<<"["<<trigPathv<<"] >> "<<endl;
    for (const string& m : savedTags) {
      if (m[0]=='-') {continue;}
      cout<<"  "<<m<<"\t"<< hltConfig_.moduleType(m)<<"\t"<<hltConfig_.moduleEDMType(m)<<endl;
    }
    cout<<endl;
  }

  /* trigger objects retrivement */
  size_t filterIndexL1 = trigEventH->filterIndex(l1trigFilter);
  size_t filterIndexL2 = trigEventH->filterIndex(l2trigFilter);
  
  const trigger::TriggerObjectCollection& allTriggerObjects(trigEventH->getObjects());
  trigger::TriggerObjectCollection selectedObjectsL1{}, selectedObjectsL2{};
  if (filterIndexL1 < trigEventH->sizeFilters()) {
    const trigger::Keys& trigKeys = trigEventH->filterKeys(filterIndexL1);
    const trigger::Vids& trigTypes= trigEventH->filterIds(filterIndexL1);

    if (verbose_) { cout<<"L1 keys size: "<<trigKeys.size()<<" L1 trig types size: "<<trigTypes.size()<<endl; }
    //assert(trigKeys.size() == trigTypes.size());
    if (verbose_) { cout<<"L1 TO keys "; }
    for (trigger::Keys::const_iterator keyIt = trigKeys.begin();
         keyIt != trigKeys.end(); ++keyIt) {
        selectedObjectsL1.push_back(allTriggerObjects[*keyIt]);
        if (verbose_) { cout<<*keyIt<<" "; }
    }
    if (verbose_) {cout<<endl;}
    
    if (verbose_) {
      cout<< " L1 TO types: ";
      for (trigger::Vids::const_iterator typeIt = trigTypes.begin();
           typeIt != trigTypes.end(); ++typeIt) {
        cout<<*typeIt<<" ";
      }
      cout<<endl;
    }
  }
  if (filterIndexL2 < trigEventH->sizeFilters()) {
    const trigger::Keys& trigKeys = trigEventH->filterKeys(filterIndexL2);
    const trigger::Vids& trigTypes= trigEventH->filterIds(filterIndexL1);
    if (verbose_) { cout<<"L2 keys size: "<<trigKeys.size()<<" L2 trig types size: "<<trigTypes.size(); }
    //assert(trigKeys.size() == trigTypes.size());
    if (verbose_) { cout<<"L2 TO keys "; }
    for (trigger::Keys::const_iterator keyIt = trigKeys.begin();
         keyIt != trigKeys.end(); ++keyIt) {
      selectedObjectsL2.push_back(allTriggerObjects[*keyIt]);
      if (verbose_) { cout<<*keyIt<<" "; }
    }
    if (verbose_) { cout<<endl; }

    if (verbose_) {
      cout<< " L2 TO types: ";
      for (trigger::Vids::const_iterator typeIt = trigTypes.begin();
           typeIt != trigTypes.end(); ++typeIt) {
        cout<<*typeIt<<" ";
      }
      cout<<endl;
    }

  }

  if (verbose_) {
    for (size_t iC(0); iC!=trigEventH->sizeCollections(); ++iC) {
      cout<<"  "<<iC<<" coll tag: "<<trigEventH->collectionTagEncoded(iC)<<" coll key:"<<trigEventH->collectionKey(iC)<<endl;
    }
  }

  // safety check
  if (selectedObjectsL1.size()==0 or selectedObjectsL2.size()==0) { return; }
  //cout<<"L1 TO: "<<selectedObjectsL1.size()<<" L2 TO: "<<selectedObjectsL2.size()<<" ++ ";

  /* trigger objects matching */
  vector<int> L1fMatchedIdx{}, L2fMatchedIdx{};
  for (size_t iMu(0); iMu!=MuTkRef.size(); ++iMu) {
    charge = MuTkRef[iMu]->charge();
    pt     = MuTkRef[iMu]->pt();
    dxy    = MuTkRef[iMu]->dxy();
    eta    = MuTkRef[iMu]->eta();
    phi    = MuTkRef[iMu]->phi();
    double vx = MuTkRef[iMu]->vx();
    double vy = MuTkRef[iMu]->vy();
    vxy = std::sqrt(vx*vx + vy*vy);

    // L1 filter
    int L1fidx(-1), bestL1fidx(-1);
    float L1fBestdr(9999.);
    for (const auto& to : selectedObjectsL1) {
      L1fidx++;
      if (find(L1fMatchedIdx.begin(), L1fMatchedIdx.end(), L1fidx) != L1fMatchedIdx.end()) {continue;}
      if (!triggerMatch(to, MuTkRef[iMu])) { continue; }
      float dr = deltaR(to, *MuTkRef[iMu].get());
      if (dr < L1fBestdr) {
        L1fBestdr = dr;
        bestL1fidx = L1fidx;
      }
    }

    if (bestL1fidx == -1) continue;
    L1fMatchedIdx.push_back(bestL1fidx);
    L1fpt  = selectedObjectsL1[bestL1fidx].pt();
    L1feta = selectedObjectsL1[bestL1fidx].eta();
    L1fphi = selectedObjectsL1[bestL1fidx].phi();

    // L2 filter
    int L2fidx(-1), bestL2fidx(-1);
    float L2fBestdr(9999.);
    for (const auto& to : selectedObjectsL2) {
      L2fidx++;
      if (find(L2fMatchedIdx.begin(), L2fMatchedIdx.end(), L2fidx) != L2fMatchedIdx.end()) {continue;}
      if (!triggerMatch(to, MuTkRef[iMu])) { continue; }
      float dr = deltaR(to, *MuTkRef[iMu].get());
      if (dr < L2fBestdr) {
        L2fBestdr = dr;
        bestL2fidx = L2fidx;
      }
    }

    if (bestL2fidx == -1) continue;
    L2fMatchedIdx.push_back(bestL2fidx);
    L2fpt  = selectedObjectsL2[bestL2fidx].pt();
    L2feta = selectedObjectsL2[bestL2fidx].eta();
    L2fphi = selectedObjectsL2[bestL2fidx].phi();


    muTree->Fill();

  }
  //cout<<"num matched L1f: "<<L1fMatchedIdx.size()<<"num matched L2f: "<<L2fMatchedIdx.size()<<endl;

}


void
TriggerMatchAnalyzer::beginJob()
{
  
  muTree = fs->make<TTree>("mutree","");

  muTree->Branch("RunNumber",   &runNumber_,         "RunNumber/I");
  muTree->Branch("LumiBlock",   &lumiBlock_,         "LumiBlock/I");
  muTree->Branch("EventNumber", &eventNumber_,       "EventNumber/l");
  muTree->Branch("NMuon",       &nMuonInCollection_, "NMuon/I");

  muTree->Branch("charge", &charge, "charge/I");
  muTree->Branch("pt",     &pt,     "pt/D");
  muTree->Branch("eta",    &eta,    "eta/D");
  muTree->Branch("phi",    &phi,    "phi/D");
  muTree->Branch("dxy",    &dxy,    "dxy/D");
  muTree->Branch("vxy",    &vxy,    "vxy/D");
  muTree->Branch("L1fpt",  &L1fpt,  "L1fpt/D");
  muTree->Branch("L1feta", &L1feta, "L1feta/D");
  muTree->Branch("L1fphi", &L1fphi, "L1fphi/D");
  muTree->Branch("L2fpt",  &L2fpt,  "L2fpt/D");
  muTree->Branch("L2feta", &L2feta, "L2feta/D");
  muTree->Branch("L2fphi", &L2fphi, "L2fphi/D");

}

void
TriggerMatchAnalyzer::endJob() {}

#include "FWCore/ParameterSet/interface/ConfigurationDescriptions.h"
#include "FWCore/ParameterSet/interface/ParameterSetDescription.h"
void
TriggerMatchAnalyzer::fillDescriptions(edm::ConfigurationDescriptions& descriptions)
{
  edm::ParameterSetDescription desc;
  desc.addUntracked<std::string>("_trigPath", "HLT_DoubleL2Mu23NoVtx_2Cha");
  desc.add<edm::InputTag>("_trigEvent", edm::InputTag("hltTriggerSummaryAOD","","HLT"));
  desc.addUntracked<edm::InputTag>("_L1TrigFilter", edm::InputTag("hltL1fL1sDoubleMu155ORTripleMu444L1Filtered0","","HLT"));
  desc.addUntracked<edm::InputTag>("_L2TrigFilter", edm::InputTag("hltL2DoubleMu23NoVertexL2Filtered2Cha","","HLT"));
  desc.add<edm::InputTag>("_muons", edm::InputTag("displacedStandAloneMuons"));
  desc.addUntracked<unsigned int>("nmuons", 2);
  desc.addUntracked<std::string>("processName", "HLT");
  desc.add<edm::InputTag>("_trigResults", edm::InputTag("TriggerResults","","HLT"));
  desc.addUntracked<bool>("verbose", false);
  descriptions.add("trigmatchana", desc);
}

#include "FWCore/Framework/interface/MakerMacros.h"
DEFINE_FWK_MODULE(TriggerMatchAnalyzer);
