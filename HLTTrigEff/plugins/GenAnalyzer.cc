#include <memory>
#include <map>

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "DataFormats/Common/interface/TriggerResults.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/HepMCCandidate/interface/GenParticleFwd.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/TrackReco/interface/TrackFwd.h"

#include "TTree.h"
#include "TLorentzVector.h"

#define M_MU 0.10565837
#define M_Z 91.1876

class GenAnalyzer : public edm::one::EDAnalyzer<edm::one::SharedResources>
{
public:
  explicit GenAnalyzer(const edm::ParameterSet&);
  ~GenAnalyzer();

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

  struct theMu
  {
    theMu() {};

    template<typename T>
    theMu(const T& m) {
      _pt =  m->pt();
      _eta = m->eta();
      _phi = m->phi();
      _lxy = m->vertex().rho();
      _lz  = m->vertex().Z();
      _passTrigger = false;
    }

    void setTriggerPass(bool p) {_passTrigger = p;}
    
    double _pt, _eta, _phi, _lxy, _lz;
    bool _passTrigger;
  };

private:
  virtual void beginJob() override;
  virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
  virtual void endJob() override;

  void branchTTree(TTree*, theMu& m);
  
  const edm::EDGetTokenT<edm::TriggerResults> trigResults;
  const std::string trigPath;
  const edm::EDGetTokenT<trigger::TriggerEvent> trigEvent;
  // const std::vector<std::string> trigFilters;

  const edm::EDGetTokenT<reco::GenParticleCollection> genMuons;
  const edm::EDGetTokenT<reco::TrackCollection> dsaMuons;

  edm::Service<TFileService> fs;
  unsigned int nmuons_;
  double mindxy_;

  std::map<std::string, theMu> _basicMu;
  std::map<std::string, TTree*> _basicMuTree;
};

void
GenAnalyzer::branchTTree(TTree* t, theMu& m)
{
  t->Branch("pt",  &m._pt,   "pt/D");
  t->Branch("eta", &m._eta,  "eta/D");
  t->Branch("phi", &m._phi,  "phi/D");
  t->Branch("lxy", &m._lxy,  "lxy/D");
  t->Branch("lz",  &m._lz,   "lz/D");
  t->Branch("passTrigger", &m._passTrigger, "passTrigger/O");
}


GenAnalyzer::GenAnalyzer(const edm::ParameterSet& iC)
:
trigResults(consumes<edm::TriggerResults>(iC.getParameter<edm::InputTag>("_trigResults"))),
trigPath(iC.getUntrackedParameter<std::string>("_trigPath")),
trigEvent(consumes<trigger::TriggerEvent>(iC.getParameter<edm::InputTag>("_trigEvent"))),
genMuons(consumes<reco::GenParticleCollection>(iC.getParameter<edm::InputTag>("_genMuons"))),
dsaMuons(consumes<reco::TrackCollection>(iC.getParameter<edm::InputTag>("_dsaMuons"))),
nmuons_(iC.getUntrackedParameter<unsigned int>("nmuons"))
{
  usesResource("TFileService");
}


GenAnalyzer::~GenAnalyzer() {}


void
GenAnalyzer::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
  using namespace edm;

  Handle<reco::GenParticleCollection> genMuonsH;
  iEvent.getByToken(genMuons, genMuonsH);

  Handle<reco::TrackCollection> dsaMuonsH;
  iEvent.getByToken(dsaMuons, dsaMuonsH);

  // if (dsaMuonsH->size() < nmuons_) return;
  std::vector<reco::TrackRef> dsaMuTkRef{};
  for (size_t iM(0); iM!=dsaMuonsH->size(); ++iM) {
    dsaMuTkRef.emplace_back(dsaMuonsH, iM);
  }

  /* general denominator selection condition */
  auto generalSelection = [](const auto& t){
    bool pass = t->pt() > 35
            && abs(t->eta()) < 2
            && t->hitPattern().numberOfValidMuonHits() > 16
            && t->hitPattern().muonStationsWithValidHits() > 1
            && t->normalizedChi2() < 10;
    return !pass;
  };
  dsaMuTkRef.erase(std::remove_if(dsaMuTkRef.begin(), dsaMuTkRef.end(), generalSelection), dsaMuTkRef.end());
  if (dsaMuTkRef.size() < nmuons_) return;

  
  Handle<edm::TriggerResults> trigResultsH;
  iEvent.getByToken(trigResults, trigResultsH);
  Handle<trigger::TriggerEvent> trigEventH;
  iEvent.getByToken(trigEvent, trigEventH);

  const edm::TriggerNames& trigNames = iEvent.triggerNames(*trigResultsH);

  //std::cout<<"_____________________________\n";
  //for (const auto& tn : trigNames.triggerNames()) {
  //    std::cout<<tn<<std::endl;
  //}
  //std::cout<<"_____________________________\n";

  bool triggerFired(trigResultsH->accept(trigNames.triggerIndex(trigPath)));

  std::vector<reco::GenParticleRef> genMuRefs{};
  for (size_t iM(0); iM!=genMuonsH->size(); ++iM) {
    reco::GenParticleRef genMu(genMuonsH, iM);
    if (genMu->isHardProcess() && abs(genMu->pdgId())==13) {
        genMuRefs.push_back(genMu);
    }
  }

  std::vector<reco::GenParticleRef> muFromDiffVtx{};
  for (const auto& m : genMuRefs) {
    double _lxy = m->vertex().rho();
    bool exist = std::find_if(muFromDiffVtx.begin(), muFromDiffVtx.end(),
            [&_lxy](const auto& mv){return mv->vertex().rho()==_lxy;}) != muFromDiffVtx.end();
    if (!exist) {muFromDiffVtx.push_back(m);}
  }

  if (muFromDiffVtx.size()>=2) {
    _basicMu["mu1"] = theMu(muFromDiffVtx[0]);
    _basicMu["mu1"].setTriggerPass(triggerFired);
    _basicMuTree["mu1"]->Fill();

    _basicMu["mu2"] = theMu(muFromDiffVtx[1]);
    _basicMu["mu2"].setTriggerPass(triggerFired);
    _basicMuTree["mu2"]->Fill();
  }

}


void
GenAnalyzer::beginJob()
{
  _basicMuTree["mu1"] = fs->make<TTree>("mu1","");
  branchTTree(_basicMuTree["mu1"], _basicMu["mu1"]);

  _basicMuTree["mu2"] = fs->make<TTree>("mu2","");
  branchTTree(_basicMuTree["mu2"], _basicMu["mu2"]);
}

void
GenAnalyzer::endJob() {}

#include "FWCore/ParameterSet/interface/ConfigurationDescriptions.h"
#include "FWCore/ParameterSet/interface/ParameterSetDescription.h"
void
GenAnalyzer::fillDescriptions(edm::ConfigurationDescriptions& descriptions)
{
  edm::ParameterSetDescription desc;
  desc.add<edm::InputTag>("_trigResults", edm::InputTag("TriggerResults","","HLT"));
  desc.add<edm::InputTag>("_trigEvent",   edm::InputTag("hltTriggerSummaryAOD","","HLT"));
  desc.add<edm::InputTag>("_genMuons", edm::InputTag("genParticles"));
  desc.add<edm::InputTag>("_dsaMuons",    edm::InputTag("displacedStandAloneMuons"));

  desc.addUntracked<std::string>("_trigPath", "HLT_DoubleL2Mu25NoVtx_2Cha_v4");
  desc.addUntracked<unsigned int>("nmuons", 2);
  descriptions.add("genana", desc);
}

#include "FWCore/Framework/interface/MakerMacros.h"
DEFINE_FWK_MODULE(GenAnalyzer);
