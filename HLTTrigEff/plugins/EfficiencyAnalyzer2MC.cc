/**
 * EDAnalyzer for MC
 * _________________
 * Examine HLT_DoubleMu25NoVtx_2Cha as a function of dR
 * requiring ONLY 2 muons passing the selection, and calculate
 * dR between those 2.
 **/


#include <memory>

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "DataFormats/Common/interface/TriggerResults.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/HLTReco/interface/TriggerTypeDefs.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/TrackReco/interface/TrackFwd.h"
#include "HLTrigger/HLTcore/interface/HLTConfigProvider.h"
#include "DataFormats/Math/interface/deltaR.h"

#include "TTree.h"
#include "TLorentzVector.h"

#define M_MU 0.10565837
#define M_Z 91.1876

class EfficiencyAnalyzer2MC : public edm::one::EDAnalyzer<edm::one::WatchRuns, edm::one::SharedResources>
{
public:
  explicit EfficiencyAnalyzer2MC(const edm::ParameterSet&);
  ~EfficiencyAnalyzer2MC();

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
  virtual void beginJob() override;
  virtual void beginRun(edm::Run const &, edm::EventSetup const&) override;
  virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
  virtual void endRun(edm::Run const &, edm::EventSetup const&) override;
  virtual void endJob() override;

  const edm::EDGetTokenT<edm::TriggerResults> trigResults;
  const edm::EDGetTokenT<trigger::TriggerEvent> trigEvent;
  const std::string trigPath;
  std::string trigPathv;

  const edm::EDGetTokenT<reco::TrackCollection> muons;
  const std::string processName_;
  const bool verbose_;

  edm::Service<TFileService> fs;
  HLTConfigProvider hltConfig_;

  TTree *muTree;
  TH1I *myCount;
  
  Int_t runNumber_, lumiBlock_;
  Long64_t eventNumber_;

  bool triggerFired, bothMatched;
  double tagMuPt, tagMuEta, tagMuPhi, tagMuDxy, tagMuVxy;
  double probeMuPt, probeMuEta, probeMuPhi, probeMuDxy, probeMuVxy;
  // bool probeMuMatch, fromZ;
  float tnpdr, tnpInvM;
};

/// ctor and dtor
EfficiencyAnalyzer2MC::EfficiencyAnalyzer2MC(const edm::ParameterSet& iC)
:
trigResults(consumes<edm::TriggerResults>(iC.getParameter<edm::InputTag>("_trigResults"))),
trigEvent(consumes<trigger::TriggerEvent>(iC.getParameter<edm::InputTag>("_trigEvent"))),
trigPath(iC.getUntrackedParameter<std::string>("_trigPath")),
muons(consumes<reco::TrackCollection>(iC.getParameter<edm::InputTag>("_muons"))),
processName_(iC.getUntrackedParameter<std::string>("processName", "HLT")),
verbose_(iC.getUntrackedParameter<bool>("_verbose", false))
{
  usesResource("TFileService");
}


EfficiencyAnalyzer2MC::~EfficiencyAnalyzer2MC() {}

void
EfficiencyAnalyzer2MC::beginJob() {
  // making tree...
  muTree = fs->make<TTree>("mu","");

  muTree->Branch("run",   &runNumber_,  "run/I");
  muTree->Branch("lumi",  &lumiBlock_,  "lumi/I");
  muTree->Branch("event", &eventNumber_,"event/l");

  muTree->Branch("triggerFired", &triggerFired, "triggerFired/O");
  muTree->Branch("bothMatched", &bothMatched, "bothMatched/O");
  muTree->Branch("tagMuPt",  &tagMuPt,  "tagMuPt/D");
  muTree->Branch("tagMuEta", &tagMuEta, "tagMuEta/D");
  muTree->Branch("tagMuPhi", &tagMuPhi, "tagMuPhi/D");
  muTree->Branch("tagMuDxy", &tagMuDxy, "tagMuDxy/D");
  muTree->Branch("tagMuVxy", &tagMuVxy, "tagMuVxy/D");

  muTree->Branch("probeMuPt",  &probeMuPt,  "probeMuPt/D");
  muTree->Branch("probeMuEta", &probeMuEta, "probeMuEta/D");
  muTree->Branch("probeMuPhi", &probeMuPhi, "probeMuPhi/D");
  muTree->Branch("probeMuDxy", &probeMuDxy, "probeMuDxy/D");
  muTree->Branch("probeMuVxy", &probeMuVxy, "probeMuVxy/D");

  // muTree->Branch("probeMuMatch", &probeMuMatch, "probeMuMatch/O");
  // muTree->Branch("fromZ", &fromZ, "fromZ/O");
  muTree->Branch("tnpdr", &tnpdr, "tnpdr/F");
  muTree->Branch("tnpInvM", &tnpInvM, "tnpInvM/F");

  myCount = fs->make<TH1I>("myCount", "Events counting", 4, 0, 4);
}

void
EfficiencyAnalyzer2MC::beginRun(edm::Run const &iRun, edm::EventSetup const& iSetup)
{
  using namespace std;
  using namespace edm;

  bool changed(true);
  if (hltConfig_.init(iRun,iSetup,processName_,changed)) {
    if (changed) {
      LogInfo("EfficiencyAnalyzer2MC")<<"EfficiencyAnalyzer2MC::beginRun: "<<"hltConfig init for Run"<<iRun.run();
      hltConfig_.dump("ProcessName");
      hltConfig_.dump("GlobalTag");
      hltConfig_.dump("TableName");
    }
  } else {
    LogError("EfficiencyAnalyzer2MC")<<"EfficiencyAnalyzer2MC::analyze: config extraction failure with processName ";
  }

}

void
EfficiencyAnalyzer2MC::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
  using namespace std;
  using namespace edm;

  /* reset */
  triggerFired = false;
  bothMatched = false;
  // probeMuMatch = false;
  // fromZ = false;
  tnpdr = 9999.;

  /* event info */
  runNumber_   = iEvent.id().run();
  lumiBlock_   = iEvent.isRealData() ? iEvent.luminosityBlock() : -1;
  eventNumber_ = iEvent.id().event();

  Handle<reco::TrackCollection> muonsH;
  iEvent.getByToken(muons, muonsH);
  if (!muonsH.isValid()) { LogError("EfficiencyAnalyzer2MC")<<"reco::TrackCollection invalid!"<<endl; return; }

  vector<reco::TrackRef> muTkRef{};
  for (size_t iM(0); iM!=muonsH->size(); ++iM) {
    muTkRef.emplace_back(muonsH, iM);
  }

  /* general selection */
  auto generalSelection = [](const auto& t){
    bool pass = t->pt() > 5
            && abs(t->eta()) < 2
            && t->hitPattern().numberOfValidMuonHits() > 16
            && t->hitPattern().muonStationsWithValidHits() > 1
            && t->normalizedChi2() < 10;
    return !pass;
  };
  muTkRef.erase(std::remove_if(muTkRef.begin(), muTkRef.end(), generalSelection), muTkRef.end());
  if (muTkRef.size() != 2) return;
  myCount->Fill(0);

  tagMuPt  = muTkRef[0]->pt();
  tagMuEta = muTkRef[0]->eta();
  tagMuPhi = muTkRef[0]->phi();
  tagMuDxy = muTkRef[0]->dxy();
  tagMuVxy = sqrt(muTkRef[0]->vx()*muTkRef[0]->vx() + muTkRef[0]->vy()*muTkRef[0]->vy());
  TLorentzVector muTagLv;
  muTagLv.SetPxPyPzE(muTkRef[0]->px(), muTkRef[0]->py(), muTkRef[0]->pz(), sqrt(muTkRef[0]->p()*muTkRef[0]->p() + M_MU*M_MU));

  probeMuPt  = muTkRef[1]->pt();
  probeMuEta = muTkRef[1]->eta();
  probeMuPhi = muTkRef[1]->phi();
  probeMuDxy = muTkRef[1]->dxy();
  probeMuVxy = sqrt(muTkRef[1]->vx()*muTkRef[1]->vx() + muTkRef[1]->vy()*muTkRef[1]->vy());
  TLorentzVector muProbeLv;
  muProbeLv.SetPxPyPzE(muTkRef[1]->px(), muTkRef[1]->py(), muTkRef[1]->pz(), sqrt(muTkRef[1]->p()*muTkRef[1]->p() + M_MU*M_MU));

  tnpInvM = (muTagLv+muProbeLv).M();
  tnpdr = deltaR(*muTkRef[0].get(), *muTkRef[1].get());

  if (verbose_) { cout<<endl; }
  if (verbose_) { cout<<"Event "<<eventNumber_<<" == "; }
  if (verbose_) { cout<<"Num mu pass general selection: "<<muTkRef.size()<<endl; }

  auto sort_by_pt_gt = [](const auto& lhs, const auto&rhs){ return lhs->pt() > rhs->pt(); };
  std::sort(muTkRef.begin(), muTkRef.end(), sort_by_pt_gt);

  /* selecting trigger objects */
  Handle<edm::TriggerResults> trigResultsH;
  iEvent.getByToken(trigResults, trigResultsH);
  if (!trigResultsH.isValid()) { LogError("EfficiencyAnalyzer2MC")<<"edm::TriggerResult invalid!"<<endl; return; }
  Handle<trigger::TriggerEvent> trigEventH;
  iEvent.getByToken(trigEvent, trigEventH);
  if (!trigEventH.isValid()) { LogError("EfficiencyAnalyzer2MC")<<"trigger::TriggerEvent invalid!"<<endl; return; }

  const vector<string>& pathNames = hltConfig_.triggerNames();

  /// checking if trigger fired 
  const vector<string> matchedPaths(hltConfig_.restoreVersion(pathNames, trigPath));
  if (matchedPaths.size() == 0) {
    LogError("EfficiencyAnalyzer2MC")<<"Could not find matched full trigger path with "<<trigPath<<"!"<<endl;
    return;
  }
  trigPathv = matchedPaths[0];
  if (hltConfig_.triggerIndex(trigPathv) >= hltConfig_.size()) {
    LogError("EfficiencyAnalyzer2MC")<<trigPathv<<"not found!"<<endl;
    return;
  }
  triggerFired = trigResultsH->accept(hltConfig_.triggerIndex(trigPathv));
  if (triggerFired) myCount->Fill(1);

  if (verbose_) { cout<<"  Trigger: "<<trigPathv<<" "<<int(triggerFired)<<endl; }

  if (triggerFired) {
    /// First, find out last filter index
    const trigger::size_type pathIdx = hltConfig_.triggerIndex(trigPathv);
    const vector<string>& nameModules = hltConfig_.moduleLabels(pathIdx);
    unsigned int sizeModules = nameModules.size();
    unsigned int iM = sizeModules;
    int lastFilterIdx(-1);
    while ( iM>0 ) {
      const string& nameFilter = nameModules[--iM];
      if (hltConfig_.moduleEDMType(nameFilter) != "EDFilter") { continue; }
      bool saveTags = hltConfig_.saveTags(nameFilter);
      if (!saveTags) { continue; }
      
      lastFilterIdx = trigEventH->filterIndex(InputTag(nameFilter, "", processName_));
      break;
    }
    if (lastFilterIdx == -1) { LogError("EfficiencyAnalyzer2MC")<<"NO filter found!"<<endl; }
    if (lastFilterIdx >= trigEventH->sizeFilters()) {
      LogError("EfficiencyAnalyzer2MC")<<"Last filter index "<<lastFilterIdx <<" not found!"
      << " (filter size: "<<trigEventH->sizeFilters()<<")"<<endl;
    }

    if (verbose_) { cout<< "  Index of last filter: "<<int(lastFilterIdx)<<endl; }

    /// Then, construct trigger object collection associated with last filter
    vector<trigger::size_type> muTrigObjsWithLastFilterIndices{};

    const trigger::Keys& keys = trigEventH->filterKeys(lastFilterIdx);
    const trigger::Vids& types= trigEventH->filterIds(lastFilterIdx);

    for (size_t iK(0); iK!=keys.size(); ++iK) {
      if (types[iK]!=trigger::TriggerObjectType::TriggerL1Mu and
          types[iK]!=trigger::TriggerObjectType::TriggerMuon) { continue; }
      if (find(muTrigObjsWithLastFilterIndices.begin(), muTrigObjsWithLastFilterIndices.end(), keys[iK])
          != muTrigObjsWithLastFilterIndices.end()) { continue; }
      muTrigObjsWithLastFilterIndices.push_back(keys[iK]);
    }

    if (verbose_) { cout<<"  Size of total mu trigger objects: "<<muTrigObjsWithLastFilterIndices.size()<<endl; }
    //if (muTrigObjsWithLastFilterIndices.size() != 2) {
    //  LogError("EfficiencyAnalyzer2MC")<<"ERROR! Number of muon trigger objects is "<<
    //  muTrigObjsWithLastFilterIndices.size()<<endl;
    //}
    if (muTrigObjsWithLastFilterIndices.size() >= 2) myCount->Fill(2);

    /* Loop reco muon tracks to find tag and probe muons */
    vector<unsigned int> tagMuIds{};
    for (const auto& mutag : muTkRef) {
      /// tag muons need to match with trigger object
      bool tagMatch(false);
      for (const trigger::size_type& toIdx : muTrigObjsWithLastFilterIndices) {
        float dr = deltaR( (trigEventH->getObjects())[toIdx], *mutag.get());
        if (dr >= 0.3) { continue; }
        tagMatch = true;
        break;
      }
      if (!tagMatch) { continue; }
      tagMuIds.push_back(mutag.key());

      /// look for any probe muon
      for (const auto& muprobe : muTkRef) {
        if (muprobe == mutag) { continue; }
        if (find(tagMuIds.begin(), tagMuIds.end(), muprobe.key()) != tagMuIds.end()) { continue; }

        /// In this case, "probe" muon also need to match trigger object
        bool probeMatch(false);
        for (const trigger::size_type& toIdx : muTrigObjsWithLastFilterIndices) {
          float dr = deltaR( (trigEventH->getObjects())[toIdx], *muprobe.get());
          if (dr >= 0.3) { continue; }
          probeMatch = true;
          break;
        }
        if (!probeMatch) { continue; }

        if (verbose_) { cout<<"both reco muons matched with trigger objects."<<endl; }
        bothMatched = true;
        myCount->Fill(3);

      }
    }
  }

  muTree->Fill();

  return;
}


void
EfficiencyAnalyzer2MC::endRun(edm::Run const &iRun, edm::EventSetup const& iSetup) {
  myCount->GetXaxis()->SetBinLabel(1, "Exact 2 reco muons");
  myCount->GetXaxis()->SetBinLabel(2, "Trigger fired");
  myCount->GetXaxis()->SetBinLabel(3, "Exact 2 trigger objects");
  myCount->GetXaxis()->SetBinLabel(4, "Both reco muons match trigger object");
}

void
EfficiencyAnalyzer2MC::endJob() {}


#include "FWCore/ParameterSet/interface/ConfigurationDescriptions.h"
#include "FWCore/ParameterSet/interface/ParameterSetDescription.h"
void
EfficiencyAnalyzer2MC::fillDescriptions(edm::ConfigurationDescriptions& descriptions)
{
  edm::ParameterSetDescription desc;
  desc.addUntracked<std::string>("_trigPath", "HLT_DoubleL2Mu23NoVtx_2Cha");
  desc.add<edm::InputTag>("_trigEvent", edm::InputTag("hltTriggerSummaryAOD","","HLT"));
  desc.add<edm::InputTag>("_muons", edm::InputTag("displacedStandAloneMuons"));
  desc.addUntracked<std::string>("processName", "HLT");
  desc.addUntracked<bool>("_verbose", false);
  desc.add<edm::InputTag>("_trigResults", edm::InputTag("TriggerResults","","HLT"));
  descriptions.add("effiana2MC", desc);
}

#include "FWCore/Framework/interface/MakerMacros.h"
DEFINE_FWK_MODULE(EfficiencyAnalyzer2MC);
