#include <memory>
#include <map>

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "DataFormats/Common/interface/TriggerResults.h"
#include "FWCore/Common/interface/TriggerNames.h"
#include "DataFormats/HLTReco/interface/TriggerObject.h"
#include "DataFormats/HLTReco/interface/TriggerEvent.h"
#include "DataFormats/MuonReco/interface/Muon.h"
#include "DataFormats/MuonReco/interface/MuonFwd.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/TrackReco/interface/TrackFwd.h"
#include "DataFormats/BeamSpot/interface/BeamSpot.h"

#include "TTree.h"
#include "TLorentzVector.h"

#define M_MU 0.10565837
#define M_Z 91.1876

class EfficiencyAnalyzer : public edm::one::EDAnalyzer<edm::one::SharedResources>
{
public:
  explicit EfficiencyAnalyzer(const edm::ParameterSet&);
  ~EfficiencyAnalyzer();

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

  struct theMu
  {
    theMu() {};

    template<typename T>
    theMu(const T& m) {
      _pt = m->pt();
      _eta = m->eta();
      _phi = m->phi();
      _dxy = m->dxy();
      _dz = m->dz();
      _vxy = std::sqrt(m->vx()*m->vx() + m->vy()*m->vy());
      _vz = m->vz();
      _innerxy = std::sqrt(m->innerPosition().X()*m->innerPosition().X()
                         + m->innerPosition().Y()*m->innerPosition().Y());
      _innerz = m->innerPosition().Z();
      _nChi2 = m->normalizedChi2();
      _dxyError = m->dxyError();
      _passTrigger = false;
      _passMassCut = false;
      _triggerMatch = false;
      _recoMuMatch = false;
      _firstHitDistance_xy = -1;
      _firstHitDistance_z = -1;
    }

    void setTriggerPass(bool p) {_passTrigger = p;}
    void setMassCutPass(bool p) {_passMassCut = p;}

    void setFirstHitDistance(std::pair<double, double> d) {
        _firstHitDistance_xy = d.first;
        _firstHitDistance_z  = d.second;
    }
    void setTriggerMatch(bool m) {_triggerMatch = m;}
    void setRecoMuMatch(bool m) {_recoMuMatch = m;}
    void setInnerXy(double v) {_innerxy = v;}
    void setInnerZ(double v) {_innerz = v;}
    void setDxy(double v) {_dxy = v;}

    bool getTriggerMatch() const {return _triggerMatch;}
    
    double _pt, _eta, _phi, _dxy, _dz, _vxy, _vz, _nChi2, _dxyError, 
           _firstHitDistance_xy, _firstHitDistance_z, _innerxy, _innerz;
    bool _passTrigger, _passMassCut, _triggerMatch, _recoMuMatch;
  };

private:
  virtual void beginJob() override;
  virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
  virtual void endJob() override;

  void branchTTree(TTree*, theMu& m);
  std::pair<double, double> distance(const math::XYZPoint&, const math::XYZPoint&) const;
  template<typename P, typename T>
  bool objMatch(const P&, const T&) const;

  const edm::EDGetTokenT<edm::TriggerResults> trigResults;
  std::string trigPath;
  const edm::EDGetTokenT<trigger::TriggerEvent> trigEvent;
  // const std::vector<std::string> trigFilters;

  const edm::EDGetTokenT<reco::MuonCollection> recoMuons;
  const edm::EDGetTokenT<reco::TrackCollection> dsaMuons;
  const edm::EDGetTokenT<reco::BeamSpot> beamSpot;
  const edm::InputTag trigFilter;

  edm::Service<TFileService> fs;
  unsigned int nmuons_;
  double mindxy_;

  std::map<std::string, theMu> _basicMu;
  std::map<std::string, TTree*> _basicMuTree;

  TTree* eventTree;

  Int_t nMuonInCollection_;
  Int_t runNumber_, lumiBlock_;
  Long64_t eventNumber_;
};

void
EfficiencyAnalyzer::branchTTree(TTree* t, theMu& m)
{
  t->Branch("pt",  &m._pt,   "pt/D");
  t->Branch("eta", &m._eta,  "eta/D");
  t->Branch("phi", &m._phi,  "phi/D");
  t->Branch("dxy", &m._dxy,  "dxy/D");
  t->Branch("dz",  &m._dz,   "dz/D");
  t->Branch("vxy", &m._vxy,  "vxy/D");
  t->Branch("vz",  &m._vz,   "vz/D");
  t->Branch("innerxy", &m._innerxy, "innerxy/D");
  t->Branch("innerz",  &m._innerz,  "innerz/D");
  t->Branch("nChi2",    &m._nChi2,    "nChi2/D");
  t->Branch("dxyError", &m._dxyError, "dxyError/D");
  t->Branch("passTrigger", &m._passTrigger, "passTrigger/O");
  t->Branch("passMassCut", &m._passMassCut, "passMassCut/O");
  t->Branch("triggerMatch", &m._triggerMatch, "triggerMatch/O");
  t->Branch("recoMuMatch", &m._recoMuMatch, "recoMuMatch/O");
  t->Branch("firstHitDis_xy", &m._firstHitDistance_xy, "firstHitDis_xy/D");
  t->Branch("firstHitDis_z" , &m._firstHitDistance_z,  "firstHitDis_z/D");
}

std::pair<double, double>
EfficiencyAnalyzer::distance(const math::XYZPoint& p0, const math::XYZPoint& p1) const
{
    double dx = p0.X()-p1.X();
    double dy = p0.Y()-p1.Y();
    double dz = p0.Z()-p1.Z();
    return std::make_pair(std::sqrt(dx*dx + dy*dy), abs(dz));
}

template<typename P, typename T>
bool
EfficiencyAnalyzer::objMatch(const P& to, const T& obj) const
{
  double deta = to.eta()-obj->eta();
  double dphi = to.phi()-obj->phi();
  double dR = std::sqrt(deta*deta + dphi*dphi);
  double dPtRel = abs(to.pt()-obj->pt())/obj->pt();

  return (dR<0.1 && dPtRel<1);
}

EfficiencyAnalyzer::EfficiencyAnalyzer(const edm::ParameterSet& iC)
:
trigResults(consumes<edm::TriggerResults>(iC.getParameter<edm::InputTag>("_trigResults"))),
trigPath(iC.getUntrackedParameter<std::string>("_trigPath")),
trigEvent(consumes<trigger::TriggerEvent>(iC.getParameter<edm::InputTag>("_trigEvent"))),
recoMuons(consumes<reco::MuonCollection>(iC.getParameter<edm::InputTag>("_recoMuons"))),
dsaMuons(consumes<reco::TrackCollection>(iC.getParameter<edm::InputTag>("_dsaMuons"))),
beamSpot(consumes<reco::BeamSpot>(iC.getParameter<edm::InputTag>("_beamspot"))),
trigFilter(iC.getUntrackedParameter<edm::InputTag>("_trigFilter")),
nmuons_(iC.getUntrackedParameter<unsigned int>("nmuons")),
mindxy_(iC.getUntrackedParameter<double>("mindxy"))
{
  usesResource("TFileService");
}


EfficiencyAnalyzer::~EfficiencyAnalyzer() {}


void
EfficiencyAnalyzer::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
  using namespace edm;

  Handle<reco::BeamSpot> beamSpotH;
  iEvent.getByToken(beamSpot, beamSpotH);

  Handle<reco::MuonCollection> recoMuonsH;
  iEvent.getByToken(recoMuons, recoMuonsH);

  Handle<reco::TrackCollection> dsaMuonsH;
  iEvent.getByToken(dsaMuons, dsaMuonsH);

  // if (dsaMuonsH->size() < nmuons_) return;
  std::vector<reco::TrackRef> dsaMuTkRef{};
  for (size_t iM(0); iM!=dsaMuonsH->size(); ++iM) {
    dsaMuTkRef.emplace_back(dsaMuonsH, iM);
  }

  /* general denominator selection condition */
  auto generalSelection = [](const auto& t){
    bool pass = t->pt() > 5
            && abs(t->eta()) < 2
            && t->hitPattern().numberOfValidMuonHits() > 16
            && t->hitPattern().muonStationsWithValidHits() > 1
            && t->normalizedChi2() < 10;
    return !pass;
  };
  dsaMuTkRef.erase(std::remove_if(dsaMuTkRef.begin(), dsaMuTkRef.end(), generalSelection), dsaMuTkRef.end());
  if (dsaMuTkRef.size() < nmuons_) return;

  auto sort_by_pt_gt = [](const auto& lhs, const auto&rhs){return lhs->pt()>rhs->pt();};
  std::sort(dsaMuTkRef.begin(), dsaMuTkRef.end(), sort_by_pt_gt);

  Handle<edm::TriggerResults> trigResultsH;
  iEvent.getByToken(trigResults, trigResultsH);
  Handle<trigger::TriggerEvent> trigEventH;
  iEvent.getByToken(trigEvent, trigEventH);

  const edm::TriggerNames& trigNames = iEvent.triggerNames(*trigResultsH);

  //std::cout<<"_____________________________\n";
  //for (const auto& tn : trigNames.triggerNames()) {
  //    std::cout<<tn<<std::endl;
  //}
  //std::cout<<"_____________________________\n";

  for (const auto& tn : trigNames.triggerNames()) {
    if (tn.substr(0, trigPath.size()) == trigPath) {
        trigPath = tn;
        break;
    }
  }

  bool triggerFired(trigResultsH->accept(trigNames.triggerIndex(trigPath)));
  size_t filterIndex = trigEventH->filterIndex(trigFilter);
  
  const trigger::TriggerObjectCollection& allTriggerObjects(trigEventH->getObjects());
  trigger::TriggerObjectCollection selectedObjects{};
  if (filterIndex < trigEventH->sizeFilters()) {
    const trigger::Keys& trigKeys = trigEventH->filterKeys(filterIndex);
    for (trigger::Keys::const_iterator keyIt = trigKeys.begin();
         keyIt != trigKeys.end(); ++keyIt) {
        selectedObjects.push_back(allTriggerObjects[*keyIt]);
    }
  }

  if (nmuons_ > 1) {
    bool massCutPassed(false);

    for (size_t iMu(0); iMu!=dsaMuTkRef.size(); ++iMu) {
      reco::TrackRef muA(dsaMuTkRef[iMu]);
      TLorentzVector muAlv;//(muA->momentum(), std::sqrt(muA->p()*muA->p() + M_MU*M_MU));
      muAlv.SetPxPyPzE(muA->px(), muA->py(), muA->pz(), std::sqrt(muA->p()*muA->p() + M_MU*M_MU));
      for (size_t jMu(iMu+1); jMu!=dsaMuTkRef.size(); ++jMu) {
        reco::TrackRef muB(dsaMuTkRef[jMu]);
        TLorentzVector muBlv;//(muB->momentum(), std::sqrt(muB->p()*muB->p() + M_MU*M_MU));
        muBlv.SetPxPyPzE(muB->px(), muB->py(), muB->pz(), std::sqrt(muA->p()*muA->p() + M_MU*M_MU));

        if (muA->charge() * muB->charge() > 0) continue;
        if (abs(M_Z - (muAlv+muBlv).M()) > 10) continue;
        massCutPassed = true;
        goto afterLoop;
      }
    }
    afterLoop:

    _basicMu["leading"] = theMu(dsaMuTkRef[0]); // leading mu
    _basicMu["leading"].setFirstHitDistance(
            distance(beamSpotH->position(), dsaMuTkRef[0]->innerPosition())
            );
    _basicMu["leading"].setTriggerPass(triggerFired);
    _basicMu["leading"].setMassCutPass(massCutPassed);
    for (const auto& to : selectedObjects) {
        if (objMatch(to, dsaMuTkRef[0])) {
            _basicMu["leading"].setTriggerMatch(true);
            break;
        }
    }
    for (const auto& rMu : *recoMuonsH) {
        if (!rMu.isGlobalMuon()) continue;
        if (objMatch(rMu, dsaMuTkRef[0])) {
            if (rMu.globalTrack().isNull()) continue;
            double _x = rMu.globalTrack()->innerPosition().X();
            double _y = rMu.globalTrack()->innerPosition().Y();
            double _z = rMu.globalTrack()->innerPosition().Z();
            double _dxy = rMu.globalTrack()->dxy();
            _basicMu["leading"].setInnerXy(std::sqrt(_x*_x + _y*_y));
            _basicMu["leading"].setInnerZ(_z);
            _basicMu["leading"].setDxy(_dxy);
            _basicMu["leading"].setRecoMuMatch(true);
            break;
        }
    }

    _basicMuTree["leading"]->Fill();


    _basicMu["subleading"] = theMu(dsaMuTkRef[1]); // subleading mu
    _basicMu["subleading"].setFirstHitDistance(
            distance(beamSpotH->position(), dsaMuTkRef[1]->innerPosition())
            );
    _basicMu["subleading"].setTriggerPass(triggerFired);
    _basicMu["subleading"].setMassCutPass(massCutPassed);
    for (const auto& to : selectedObjects) {
        if (objMatch(to, dsaMuTkRef[1])) {
            _basicMu["subleading"].setTriggerMatch(true);
            break;
        }
    }
    for (const auto& rMu : *recoMuonsH) {
        if (!rMu.isGlobalMuon()) continue;
        if (objMatch(rMu, dsaMuTkRef[0])) {
            if (rMu.globalTrack().isNull()) continue;
            const math::XYZPoint& inp = rMu.globalTrack()->innerPosition();
            double _dxy = rMu.globalTrack()->dxy();
            _basicMu["subleading"].setInnerXy(std::sqrt(inp.X()*inp.X() + inp.Y()*inp.Y()));
            _basicMu["subleading"].setInnerZ(inp.Z());
            _basicMu["subleading"].setDxy(_dxy);
            _basicMu["subleading"].setRecoMuMatch(true);
            break;
        }
    }

    _basicMuTree["subleading"]->Fill();

  }
  else {
    _basicMu["leading"] = theMu(dsaMuTkRef[0]); // leading mu
    _basicMu["leading"].setFirstHitDistance(
            distance(beamSpotH->position(), dsaMuTkRef[0]->innerPosition())
            );
    _basicMu["leading"].setTriggerPass(triggerFired);
    for (const auto& to : selectedObjects) {
        if (objMatch(to, dsaMuTkRef[0])) {
            _basicMu["leading"].setTriggerMatch(true);
            break;
        }
    }
    for (const auto& rMu : *recoMuonsH) {
        if (!rMu.isGlobalMuon()) continue;
        if (objMatch(rMu, dsaMuTkRef[0])) {
            if (rMu.globalTrack().isNull()) continue;
            const math::XYZPoint& inp = rMu.globalTrack()->innerPosition();
            double _dxy = rMu.globalTrack()->dxy();
            _basicMu["leading"].setInnerXy(std::sqrt(inp.X()*inp.X() + inp.Y()*inp.Y()));
            _basicMu["leading"].setInnerZ(inp.Z());
            _basicMu["leading"].setDxy(_dxy);
            _basicMu["leading"].setRecoMuMatch(true);
            break;
        }
    }

    _basicMuTree["leading"]->Fill();
  }

  /* event info */
  nMuonInCollection_ = dsaMuonsH->size();
  eventNumber_ = iEvent.id().event();
  lumiBlock_ = iEvent.isRealData() ? iEvent.luminosityBlock() : -1;
  runNumber_ = iEvent.id().run();
  eventTree->Fill();

}


void
EfficiencyAnalyzer::beginJob()
{
  eventTree = fs->make<TTree>("event","");
  eventTree->Branch("RunNumber",   &runNumber_, "RunNumber/I");
  eventTree->Branch("LumiBlock",   &lumiBlock_, "LumiBlock/I");
  eventTree->Branch("EventNumber", &eventNumber_, "EventNumber/l");
  eventTree->Branch("NMuon",       &nMuonInCollection_, "NMuon/I");

  _basicMuTree["leading"] = fs->make<TTree>("leadingMu","");
  branchTTree(_basicMuTree["leading"], _basicMu["leading"]);
  if (nmuons_ > 1) {
    _basicMuTree["subleading"] = fs->make<TTree>("subleadingMu","");
    branchTTree(_basicMuTree["subleading"], _basicMu["subleading"]);
  }
}

void
EfficiencyAnalyzer::endJob() {}

#include "FWCore/ParameterSet/interface/ConfigurationDescriptions.h"
#include "FWCore/ParameterSet/interface/ParameterSetDescription.h"
void
EfficiencyAnalyzer::fillDescriptions(edm::ConfigurationDescriptions& descriptions)
{
  edm::ParameterSetDescription desc;
  desc.add<edm::InputTag>("_trigResults", edm::InputTag("TriggerResults","","HLT"));
  desc.add<edm::InputTag>("_trigEvent",   edm::InputTag("hltTriggerSummaryAOD","","HLT"));
  desc.add<edm::InputTag>("_beamspot",    edm::InputTag("offlineBeamSpot"));
  desc.add<edm::InputTag>("_recoMuons",   edm::InputTag("muons"));
  desc.add<edm::InputTag>("_dsaMuons",    edm::InputTag("displacedStandAloneMuons"));

  desc.addUntracked<std::string>("_trigPath", "HLT_DoubleL2Mu23NoVtx_2Cha_v1");
  desc.addUntracked<edm::InputTag>("_trigFilter", edm::InputTag("","","HLT"));
  desc.addUntracked<unsigned int>("nmuons", 2);
  desc.addUntracked<double>("mindxy", 0.0);
  descriptions.add("effiana", desc);
}

#include "FWCore/Framework/interface/MakerMacros.h"
DEFINE_FWK_MODULE(EfficiencyAnalyzer);
